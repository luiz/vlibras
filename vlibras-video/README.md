# VLibras Video Container

## Requirements

Recommended OS: [Linux Ubuntu 14.04.4 LTS 64-bit](http://releases.ubuntu.com/14.04/).

Dependencies: [Docker](https://www.docker.com/), [RabbitMQ](https://www.rabbitmq.com/),
              [Graylog](https://www.graylog.org/), [Nodejs](https://nodejs.org/en/),
              [Redis](https://redis.io/), [MongoDB](https://www.mongodb.com/),
              [FFmpeg](https://www.ffmpeg.org/).

Aditional Dependencies: VLibras Translate, VLibras Video Player

>Note: See Installation section to install dependencies.

You need install some packages: `build-essential`, `git`, `libssl-dev`, `python-pip`,
  `python-setuptools`.

```sh
    $ sudo apt-get update
    $ sudo apt-get install -y build-essential git libssl-dev python-pip python-setuptools
```

## Sources

To get source code, use these command:

```sh
    $ git clone 'git@gitlab.lavid.ufpb.br:vlibras/vlibras-video-container.git'
```

## Installation

To install vlibras-video-container for production, use these commands:

```sh
    $ cd dependencies
    $ ./configureDocker
    $ docker build
```

If you need to install for development, use these commands:

```sh
    $ cd dependencies
    $ ./configure
```

## Execution instructions

Execute each worker in the core folder

## GPLv3 License

{\rtf1\ansi\ansicpg1252\deff0\nouicompat\deflang1046{\fonttbl{\f0\fnil\fcharset0 Calibri;}}
{\colortbl ;\red0\green0\blue255;}
{\*\generator Riched20 10.0.14393}\viewkind4\uc1
\pard\sa200\sl276\slmult1\b\f0\fs48\lang22 Licen\'e7a GPLv3\b0\fs22\par
Este arquivo esta sujeito a versao 3 da GNU General Public License, que foi adicionada nesse pacote no arquivo COPYING e esta disponivel pela Web em {{\field{\*\fldinst{HYPERLINK http://pt.wikipedia.org/wiki/GNU_General_Public_License }}{\fldrslt{http://pt.wikipedia.org/wiki/GNU_General_Public_License\ul0\cf0}}}}\f0\fs22 ; Qualquer notifica\'e7\'e3o mande um email para os mantenedores do projeto.\par
{{\field{\*\fldinst{HYPERLINK http://www.gnu.org/licenses/gpl-3.0.html }}{\fldrslt{http://www.gnu.org/licenses/gpl-3.0.html\ul0\cf0}}}}\f0\fs22\par
A GPL3 \'e9 recursiva! Com isso n\'e3o haver\'e1 prote\'e7\'e3o intelectual sobre tais sistemas! Ou seja, GPL \'e9 viral. O que significa dizer que qualquer coisa que ela toca, vira GPL. Se voc\'ea usar uma biblioteca GPL, seu aplicativo ser\'e1 GPL.\par
}

## Contributors

* Erickson Silva <erickson.silva@lavid.ufpb.br>
* Jonathan Lincoln Brilhante <jonathan.lincoln.brilhante@gmail.com>
* Wesnydy Lima Ribeiro <wesnydy@lavid.ufpb.br>
