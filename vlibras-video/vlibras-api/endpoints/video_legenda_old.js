var parameters = require('../helpers/parameters');
var properties = require('../helpers/properties');
var files = require('../helpers/files');
var core = require('../helpers/core');
var db = require('../db/api');
var queue_helper = require('../helpers/queue');
var exec = require('child_process').exec, child;
var uuid = require('node-uuid');
var mkdirp = require('mkdirp');
var async = require('async');
var _ = require('lodash');
var logger = require('../logsystem/main.js');
var url = require('url');
var requests = require('../helpers/requests');
var querystring = require('querystring');

function init(req, res, Request, queue) {
	res.set("Content-Type", "application/json");

	process(req, res, Request, queue);
}

function process(req, res, Request, queue) {
	var id = uuid.v4();
	var folder = properties.uploads_folder + id;
	var locals = {};

	var request_object = new Request({
		id: id,
		type: req.body.servico,
		status: 'Submitted',
		link: 'http://' + properties.SERVER_IP + ':' + properties.port + '/' + id + '.mp4',
		link_video: req.body.video_url,
		author: req.body.conteudista,
		subtitle: 'http://150.165.204.80:5000/api/subtitle.srt',
		dictionary: '1.0',
		created_at: new Date(),
		updated_at: new Date(),
	});

	db.create(request_object, function(result) {
		if (result !== null) {
			res.send(200, { 'id': result.id});
		} else {
			res.send(500, { 'error': 'Erro na criação da requisição.'});
		}
	});

	async.series([
		// Cria a pasta apropriada
		function(callback) {
			console.log("== Criando pasta " + folder);

			mkdirp(folder, function(err) {
				var error;

				if (err) { error = "Erro na criação da pasta com o id: " + id + "; " + err; }

				callback(error);
			});
		},
		// Baixa e move os arquivos para a pasta correta
		function(callback) {
			console.log("== Baixando os arquivos");
			try {
				setTimeout(function() { downloadAndMoveFiles(folder, req, locals, callback); }, 1000);
			} catch (err) {
				console.log(" Erro " + err);
				callback(err);
			}
		},

		// Chama o core
		function(callback) {
			console.log("== Chamando o core");
			// Faz a chamada ao core
			try {
				callCoreSubtitle(id, locals.video, locals.subtitle, req, res, Request, request_object, queue);
				callback();
			} catch (err) {
				callback(err);
			}
		}
	], function(err) {
		// Se tiver erro
		if (err) {
			res.send(500, parameters.errorMessage(err));

			return;
		}
	});
}

function downloadAndMoveFiles(folder, req, locals, callback) {
	async.parallel([
		// Download video
		function(callback) {
			console.log("Baixando video");
			files.downloadAndMoveVideo(folder, req, locals, callback);
		},

		// Download subtitle
		function(callback) {
			console.log("Baixando legenda");
			files.downloadAndMoveSubtitle(folder, req, locals, callback);
		}
	], function(err) {
		console.log("=== Legenda e video baixados");

		// Callback chamado depois de todas as tarefas
		// Se tiver erro, vai passar para cima
		callback(err);
	});
}

function callCore(id, video, subtitle, req, res, Request, request_object) {

	/* Cria a linha de comando */
	/* slice(2) é para transformar ./path em path */
	var command_line = 'vlibras_user/vlibras-core/./vlibras -V ' + video.path.slice(2) + ' -p bottom_right -r large  --id ' + id + ' --mode devel > /tmp/core_log 2>&1';

	console.log("=== Core: " + command_line);

	console.log("ID: " + request_object.id);
	core.call(id, command_line, req, res, Request, request_object, "videos");
}

function callCoreSubtitle(id, video, subtitle, req, res, Request, request_object, queue) {
               var command_line = 'vlibras_user/vlibras-core/./vlibras -V ' + video.path.slice(2) + ' -S ' +  subtitle.path.slice(2) + ' -l portugues -p '+ req.body.posicao + ' -r ' + req.body.tamanho + ' -b opaque --id ' + id + ' -m devel > /tmp/core_log 2>&1';
		console.log("Chamada ao core == " + command_line);
		
		var child;

		var job = queue.create('exec_command_line', {
		    title: 'Command Line for: ' + req.body.servico,
		    command_line: command_line,
		    callback: req.body.callback,
	            idreq: request_object.id,
		    idsolicitacao: id
		}).removeOnComplete( true ).save();
		
}

module.exports.init = init;
