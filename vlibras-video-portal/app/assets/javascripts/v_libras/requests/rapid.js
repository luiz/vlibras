/*
 * jquery-File-Upload
 */
$(function() {
    var videoFile;
    var subtitleFile;

    function uploadFiles() {
        if (subtitleFile == null) {
            files = [videoFile];
            params = ['video'];
        } else {
            files = [videoFile, subtitleFile];
            params = ['video', 'subtitle'];
        }

        $('#vlibras-rapid-form').fileupload('send',
            { files: files,
                paramName: params });
    }

    $('#vlibras-rapid-form').fileupload({
        autoUpload: false,
        singleFileUploads: false,

        always: function(e, data) {
            location.href = data.jqXHR.responseJSON.redirect_to;
        },

        add: function(e, data) {
            if (data.fileInput[0].name === 'video') {
                videoFile = data.files[0];
            } else if (data.fileInput[0].name === 'subtitle') {
                subtitleFile = data.files[0];
            };

            $("#submit-button").unbind('click').on('click', function(event) {
                $(".field").hide();
                $(".progress").show();
                $("#submit-button").hide();
                $("#vlibras-rapid h2").text("Enviando...");

                uploadFiles();

                event.preventDefault();
            });
        }
    });

    $("#vlibras-rapid-form").bind("fileuploadprogress", function (e, data) {
        var percentage = Math.round(data.loaded / data.total * 100);
        var bitrate = Math.round(data.bitrate / 8 / 1024 * 100) / 100 + " KB/s";

        $("#upload-bar").css("width", percentage + "%");
        $("#upload-label").text(percentage + "% (" + bitrate + ")");
    });
});



/*
 * Radio box service (video or video-subtitle)
 */

$(function() {

    $("#url").show();
    $("#legend").show();

    /* When user press "Back" on the browser */
    if ($("#service-video-subtitle")[0].checked) {
        $("#service-video-subtitle").click();
    }

    if ($("#service-video")[0].checked) {
        $("#service-video").click();
    }
});


/*
 * File type verification
 */

$(function() {
    $("#subtitle-upload").change(function() {
        var acceptedFileTypes = ["srt"];
        validateFile($(this), acceptedFileTypes);
    });

    $("#video-upload").change(function() {
        var acceptedFileTypes = ["flv", "ts", "avi", "mp4", "mov", "webm", "wmv", "mkv"];
        validateFile($(this), acceptedFileTypes);
    });

    function validateFile(input, acceptedFileTypes) {
        var isValidFile = checkType(input, acceptedFileTypes);

        if (!isValidFile) {
            input.val(null);
            alert("Apenas os formatos abaixo são aceitos:\n\n" + acceptedFileTypes.join(", "));
        }

        updateButton(input);

        return true;
    }

    function updateButton(input) {
        var filename = input.val().split("\\").pop();
        var buttonText = "Arquivo: " + filename;

        $("#" + input[0].id).prev(".button-text").text(buttonText).parent().addClass("btn-info");
    }
});
