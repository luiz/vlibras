function checkType(file, acceptedFileTypes) {
    var ext = file.val().split('.').pop().toLowerCase();
    var isValidFile = false;

    for (var i = 0; i < acceptedFileTypes.length; i++) {
        if (ext == acceptedFileTypes[i]) {
            isValidFile = true;
            break;
        }
    }

    return isValidFile;
}

// This must be applied to a form (or an object inside a form).
// http://stackoverflow.com/questions/2530635/jquery-add-additional-parameters-on-submit-not-ajax
jQuery.fn.addHidden = function (name, value) {
    return this.each(function () {
        var input = $("<input>").attr("type", "hidden").attr("name", name).val(value);
        $(this).append($(input));
    });
};