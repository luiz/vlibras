/*
 * jquery-File-Upload
 */
$(function() {
    var videoFile;
    var subtitleFile;

    function uploadFiles() {
        initializeFileUpload();

        if (subtitleFile == null) {
            files = [videoFile];
            params = ['video'];
        } else {
            files = [videoFile, subtitleFile];
            params = ['video', 'subtitle'];
        }

        $('#vlibras-form').fileupload('send',
            { files: files,
                paramName: params });
    }

    function initializeFileUpload() {
        $('#vlibras-form').fileupload({
            autoUpload: false,
            singleFileUploads: false,

            always: function (e, data) {
                location.href = data.jqXHR.responseJSON.redirect_to;
            },
        });

        $("#vlibras-form").bind("fileuploadprogress", function (e, data) {
            var percentage = Math.round(data.loaded / data.total * 100);
            var bitrate = Math.round(data.bitrate / 8 / 1024 * 100) / 100 + " KB/s";

            $("#upload-bar").css("width", percentage + "%");
            $("#upload-label").text(percentage + "% (" + bitrate + ")");
        });
    }


    $("#vlibras-wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "horizontal",
        enablePagination: true,
        forceMoveForward: true,

        // startIndex: 1,

        onStepChanged: stepChanged,
        onFinished: finished,

        labels: {
            cancel: "Cancelar",
            current: "etapa atual:",
            pagination: "Pagination",
            finish: "Finalizar",
            next: "Próximo",
            previous: "Anterior",
            loading: "Carregando ..."
        }
    });

    function stepChanged(event, currentIndex, priorIndex) {
        var totalSteps = $("#vlibras-wizard .content section").size();

        if ((priorIndex === 0) && (currentIndex === 1)) {
            videojs($("#video-step").find("video")[0]).pause();
            videojs($("#service-step").find("video")[0]).play();

            $("#menu").hide();
        }

        if ((priorIndex === 1) && (currentIndex === 2)) {
            videojs($("#service-step").find("video")[0]).pause();
            videojs($("#position-step").find("video")[0]).play();

            $("#menu").hide();
        }

        if ((priorIndex === 2) && (currentIndex === 3)) {
            videojs($("#position-step").find("video")[0]).pause();
            videojs($("#size-step").find("video")[0]).play();
        }

        if ((currentIndex + 1) === totalSteps) {
            $("#btn-next").text("Finalizar");
        } else {
            $("#btn-next").text("Próximo");
        }

        deactivateNextButton();
    }

    function finished(event, currentIndex) {
        $("#vlibras-wizard .content").hide();
        $("#vlibras-wizard .steps").hide();
        $("#menu").show();

        // $("#btn-next").text("Enviando... Aguarde, por favor.");
        $("#btn-next").hide();
        $(".progress").show();

        uploadFiles();

        deactivateNextButton();
    }

    $('#menu #btn-next').click(function() {
        // Number of steps
        var totalSteps = $("#vlibras-wizard .content section").size();
        var currentStep = $("#vlibras-wizard").steps('getCurrentIndex');

        if ((currentStep + 1) === totalSteps) {
            $("#vlibras-wizard").steps('finish');
        } else {
            $("#vlibras-wizard").steps('next');
        }

        return false;
    });

    function activateNextButton() {
        $("#btn-next").prop('disabled', false);
    }

    function deactivateNextButton() {
        $("#btn-next").prop('disabled', true);
    }

    $("#vlibras-wizard").find("#position-step").on("click", "a", function() {
        $("#vlibras-form").addHidden('params[posicao]', $(this).data("value"));
        $("#vlibras-form").addHidden('params[transparencia]', 'opaco');
        $("#vlibras-wizard").steps('next');

        console.debug("Posição: " + $(this).data("value"));

        return false;
    });

    $("#vlibras-wizard").find("#size-step").on("click", "a", function() {
        $("#vlibras-form").addHidden('params[tamanho]', $(this).data("value"));
        $("#vlibras-wizard").steps('finish');

        console.debug("Tamanho: " + $(this).data("value"));

        return false;
    });


    /*
     * Button events
     */
    $("#vlibras-wizard").on("click", "#btn-video-legenda", function() {
        console.debug("Serviço: video-legenda");

        $("#vlibras-form").addHidden('service', 'video-legenda');
        // $("#subtitle-upload-container").show();
        $("#subtitle-upload").click();
        // $("#menu").show();

        // $('html, body').animate({
        //     scrollTop: $("#subtitle-upload").offset().top
        // }, 600);

        return false;
    });

    $("#vlibras-wizard").on("click", "#btn-video", function() {
        console.debug("Serviço: video");

        $("#vlibras-wizard").steps('next');
        $("#vlibras-form").addHidden('service', 'video');

        return false;
    });

    $("#vlibras-wizard").on("click", "#btn-video-upload", function() {
        $("#video-upload").click();

        return false;
    });



    /*
     * Validates video and subtitle extension and activate the next button
     */
    $("#vlibras-form #subtitle-upload").change(function(event) {
        var acceptedFileTypes = ["srt"];

        if (validateFileWizard($(this), acceptedFileTypes)) {
            subtitleFile = event.target.files[0];
        }
    });

    $("#vlibras-form #video-upload").change(function(event) {
        $("#vlibras-form").addHidden('service', 'video');

        var acceptedFileTypes = ["flv", "ts", "avi", "mp4", "mov", "webm", "wmv", "mkv"];

        if (validateFileWizard($(this), acceptedFileTypes)) {
            videoFile = event.target.files[0];
        };
    });

    function validateFileWizard(input, acceptedFileTypes) {
        var isValidFile = checkType(input, acceptedFileTypes);

        if (isValidFile) {
            $("#vlibras-wizard").steps('next');
        } else {
            input.val(null);
            alert("Apenas os formatos abaixo são aceitos:\n\n" + acceptedFileTypes.join(", "));
        }

        return true;
    }
});