# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: v_libras_videos
#
#  id         :integer          not null, primary key
#  request_id :integer
#  url        :string(255)
#  created_at :datetime
#  updated_at :datetime
#  seen       :boolean
#

class VLibras::Video < ActiveRecord::Base
  belongs_to :request, :class => VLibras::Request, :dependent => :delete

  validates :request_id, :url, :presence => true

  before_validation :default_values

  scope :not_seen, -> { where(:seen => false) }

  def mark_as_seen!
    self.update!(:seen => true) unless self.seen
  end

  def thumb
    self.url.gsub('flv', 'png')
  end

  def url_mp4
    self.url.gsub('flv', 'mp4')
  end

protected
  def default_values
    self.seen = false if self.seen.nil?

    return true
  end
end
