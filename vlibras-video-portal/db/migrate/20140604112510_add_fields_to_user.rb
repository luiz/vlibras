# -*- encoding : utf-8 -*-
class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :address, :text
    add_column :users, :zipcode, :string
    add_column :users, :phone, :string
  end
end
