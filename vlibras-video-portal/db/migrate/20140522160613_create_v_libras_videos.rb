# -*- encoding : utf-8 -*-
class CreateVLibrasVideos < ActiveRecord::Migration
  def change
    create_table :v_libras_videos do |t|
      t.references :request
      t.string :url

      t.timestamps
    end
  end
end
