# -*- encoding : utf-8 -*-
class CreateVLibrasRequests < ActiveRecord::Migration
  def change
    create_table :v_libras_requests do |t|
      t.string :status
      t.string :service_type

      t.references :owner
      t.text :params

      t.text :response

      t.timestamps
    end
  end
end
