# -*- encoding : utf-8 -*-
if ENV['DOMAIN_NAME'].nil? || ENV['DOMAIN_NAME'].empty?
  raise 'DOMAIN_NAME must be set in .env (used for callback url)'
end

if ENV['VLIBRAS_API_URL'].nil? || ENV['VLIBRAS_API_URL'].empty?
  raise 'VLIBRAS_API_URL must be set in .env'
end
