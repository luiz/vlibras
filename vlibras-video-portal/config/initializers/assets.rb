# -*- encoding : utf-8 -*-
Rails.application.config.assets.precompile += %w( v_libras/requests/shared.js )
Rails.application.config.assets.precompile += %w( v_libras/requests/rapid.js )
Rails.application.config.assets.precompile += %w( v_libras/requests/new.js )
Rails.application.config.assets.precompile += %w( v_libras/requests/workflow.js )
Rails.application.config.assets.precompile += %w( v_libras/videos/index.js )
Rails.application.config.assets.precompile += %w( jquery.steps.js )
Rails.application.config.assets.precompile += %w( jquery.contra.min.js )

Rails.application.config.assets.precompile += %w( v_libras/requests.css )
Rails.application.config.assets.precompile += %w( jquery.steps.css )
