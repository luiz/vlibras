# -*- encoding : utf-8 -*-
Rails.application.routes.draw do
  devise_for :users

  constraints CanAccessDelayedJobWeb do
    namespace :admin do
      match "/delayed_job" => DelayedJobWeb, :anchor => false, via: [:get, :post]
    end
  end

  ActiveAdmin.routes(self)

  root 'static#home', as: :home

  namespace :v_libras do
    resources :requests, :only => [ :new, :create ] do
      get 'rapid', on: :collection
      post 'callback', on: :collection
    end

    resources :videos, :only => [ :index, :show, :destroy ]


  end

end
