#!/bin/bash

rake db:create db:migrate

PID="/myapp/tmp/pids/server.pid"

if [ ! -z "$DATABASE_HOST" ]; then
    sed -i "s/vlibrasdb/$DATABASE_HOST/g" $APP_HOME/config/database.yml
fi

if [ ! -z "$DATABASE_DB" ]; then
    sed -i "s/vlibrasvideo/$DATABASE_DB/g" $APP_HOME/config/database.yml
fi

if [ ! -z "$DATABASE_USER" ]; then
    sed -i "s/vlibrasuser/$DATABASE_USER/g" $APP_HOME/config/database.yml
fi

if [ ! -z "$DATABASE_PASSWORD" ]; then
    sed -i "s/vlibraspassword/$DATABASE_PASSWORD/g" $APP_HOME/config/database.yml
fi

if [ -f $PID ] ; then
    rm -f $PID
fi

exec rails server -e production --port 8080 --binding 0.0.0.0
