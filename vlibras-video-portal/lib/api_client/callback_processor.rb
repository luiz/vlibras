# -*- encoding : utf-8 -*-
class ApiClient::CallbackProcessor
  def self.process(params)
    request = VLibras::Request.find(params[:request_id])

    if params['error']
      Rails.logger.debug "[VLibras::Callback]: Error #{params}"
      request.update!(:status => 'error', :response => params['error'])
    else
      Rails.logger.debug "[VLibras::Callback] OK: #{params}"
      request.update!(:status => 'success')
      request.create_video!(:url => params['response'])
    end

    Rails.logger.debug "[VLibras::Callback] Notifying websocket channel"
  end
end
