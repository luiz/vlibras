# -*- encoding : utf-8 -*-
require 'httparty'

module ApiClient::Client
  include HTTMultiParty
  default_timeout 10 * 60


  def self.submit(request, files)
    options = process_params(request, files)
    Rails.logger.debug "[VLibras::Submit] Join to submit a new request"
    Delayed::Worker.logger.debug "[VLibras::Request] Options: #{options}"
    response = self.post(ApiClient::API_URL, options)
    Delayed::Worker.logger.debug "[VLibras::Request] Status #{response.response.code}"

    if response.response.code == '200'
      # Response is processed by callback
    else
      request.update!(:status => 'error', :response => response.body)
    end

  rescue => e
    request.update!(:status => 'error', :response => e.to_s)
  ensure
    # FIXME: Running on another thread. Websocket not working :(
    Delayed::Worker.logger.debug "[VLibras::Request] Sending message to websocket channel"
  end


  #
  # Process the params from the request AR object
  # and return options to make the post request
  #
  def self.process_params(request, files)
    options = { query: request.params.clone }
    options[:query].merge!(:servico => request.service_type)
    options[:query].merge!(:callback => "http://#{ENV['DOMAIN_NAME']}/v_libras/requests/callback?request_id=#{request.id}")

    options[:query].merge!(:video => files[:video].file.to_file)

    unless files[:subtitle].nil?
      options[:query].merge!(:legenda => files[:subtitle].file.to_file)
      options[:query].merge!(:linguagem => 'portugues')
    end

    return options
  end


private
  def self.url_with_service(service)
    URI.encode("#{ApiClient::API_URL}?servico=#{service}")
  end
end
