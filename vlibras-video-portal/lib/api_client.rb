# -*- encoding : utf-8 -*-
class ApiClient
  API_URL = ENV['VLIBRAS_API_URL']

  def self.check_status
    begin
      ApiClient::Client.post(ApiClient::API_URL)
    rescue
      return false
    end
      return true
  end
end
