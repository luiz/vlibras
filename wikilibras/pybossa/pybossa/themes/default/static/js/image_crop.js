(function(global){

    global = global || {};
    // Preview image before uploading
    var MAX_FILE_SIZE = 2000000;

    function _previewImage(){
        var oFReader = new FileReader();
        var avatar = $("#avatar")
        var avatar_file = avatar[0].files[0];
        if (avatar_file.size > MAX_FILE_SIZE) {
            alert("This file is too large. Max size allowed is 2MB. Please, select another file.");
            avatar.replaceWith(avatar = avatar.clone(true));
            return;
        }
        oFReader.readAsDataURL(avatar_file);

        oFReader.onload = function (oFREvent) {

            jQuery(function($) {
                document.getElementById("uploadPreview").src = oFREvent.target.result;
                var img = document.getElementById('uploadPreview');
                var width = img.clientWidth;
                var height = img.clientHeight;
               
            	// Check if Jcrop preview has a image and clears the previous set up
                var previewImg = $('#uploadPreview + .jcrop-holder img')[0];
                if (previewImg) {
                	width = previewImg.clientWidth;
                	height = previewImg.clientHeight; 
            		$('#uploadPreview').data('Jcrop').destroy();
            		$('#uploadPreview').removeAttr("style");
                }
                
                if (width > height) {
                    width = height - 100;
                }
                else {
                    height = width - 100;
                }
            	
                $('#uploadPreview').Jcrop({
                    onSelect:    _updateCoords,
                    onChange:    _updateCoords,
                    bgColor:     'black',
                    bgOpacity:   .4,
                    minSize: [100, 100],
                    setSelect:   [  0, 
                                    0, 
                                    (width / 2), 
                                    (height/ 2)],
                    aspectRatio: 1,
                    boxWidth: 450
                });
            });
        };
    }

    function _updateCoords(c){
        $("#x1").val(Math.floor(c.x));
        $("#y1").val(Math.floor(c.y));
        $("#x2").val(Math.floor(c.x2));
        $("#y2").val(Math.floor(c.y2));
    }

    global.previewImage = _previewImage;

}(window));
