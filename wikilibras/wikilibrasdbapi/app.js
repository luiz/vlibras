var _ = require("lodash");
var async = require("async");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var express = require("express");
var favicon = require("serve-favicon");
var http = require("http");
var logger = require("morgan");
var multer = require("multer");
var path = require("path");
var util = require("util");
var files = require(path.join(__dirname, "helpers/files"));
var routes = require(path.join(__dirname, "routes/index"));
var users = require(path.join(__dirname, "routes/users"));
var app = express();
var server = http.createServer(app);

var upload = multer(
{
    dest: "uploads/"
});

server.maxConnections = 5000;

// static path
app.use("/sinais", express.static(path.join(__dirname, "sinais")));
app.use("/avatar", express.static(path.join(__dirname, "avatar")));
app.use("/blender", express.static(path.join(__dirname, "blender")));
app.use("/public", express.static(path.join(__dirname, "public")));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, "public", "img", "favicon.ico")));
app.use(logger("dev"));
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded(
{
    extended: true
}));

app.use(cookieParser());
app.use(function(req, res, next)
{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
    next();
});

app.use("/", routes);
app.use("/users", users);

// catch 404 and forward to error handler
app.use(function(req, res, next)
{
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development")
{
    app.use(function(err, req, res, next)
    {
        res.status(err.status || 500);
        res.render("error"
        , {
            message: err.message
            , error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next)
{
    res.status(err.status || 500);
    res.render("error"
    , {
        message: err.message
        , error:
        {}
    });
});

module.exports = app;
