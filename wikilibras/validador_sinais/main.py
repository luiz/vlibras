# -*- coding: utf-8 -*-
from flask import Flask, send_from_directory
from flask.ext.cors import CORS
from jinja2 import Environment, PackageLoader
from validador import Validador
import os
import pyutil

app = Flask(__name__)
controller = None

@app.route("/update_project")
def update_project():
    try:
        return controller.update_project()
    except:
        pyutil.print_stack_trace()
        raise

@app.route("/create_project")
def create_project():
    try:
        return controller.create_project()
    except:
        pyutil.print_stack_trace()
        raise

@app.route("/finish_task", methods=["POST"])
def finish_task():
    try:
        return controller.finish_task()
    except:
        pyutil.print_stack_trace()
        raise

def read_settings(app):
    here = os.path.abspath(__file__)
    config_path = os.path.join(os.path.dirname(here), 'settings_local.py')
    if os.path.exists(config_path):
        app.config.from_pyfile(config_path)

    if app.config['APACHE_HOST']:
        app.config['HOST_ENDPOINT'] = "http://" + app.config['SERVER_HOST'] + app.config['APACHE_HOST_ENDPOINT']
        app.config['HOST_STATIC_FILES_ENDPOINT'] = "http://" + app.config['SERVER_HOST'] + app.config['APACHE_STATIC_FILES_ENDPOINT']
    else:
        app.config['HOST_ENDPOINT'] = "http://" + app.config['SERVER_HOST'] + ":" + str(app.config['SERVER_PORT'])
        app.config['HOST_STATIC_FILES_ENDPOINT'] = app.config['HOST_ENDPOINT']

def setup_controller():
    global controller
    read_settings(app)
    env = Environment(loader=PackageLoader('main', 'view/templates'))
    controller = Validador(app.config, env)
    
def setup_static_files_service(app):
    if not app.config['APACHE_HOST']:
        CORS(app)
        @app.route("/<path:path>")
        def send_static_files(path):
            root_dir = os.path.abspath(os.path.dirname(__file__))
            file_dir = os.path.join(root_dir, "view")
            return send_from_directory(file_dir, path)     

def run():
    app.run(host=app.config['SERVER_HOST'], port=app.config['SERVER_PORT'])
    
setup_controller()
setup_static_files_service(app)

if __name__ == '__main__':
    try:
        run()
    except:
        pyutil.print_stack_trace()
        raise
