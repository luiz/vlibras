(function(pybossaApiHelper, $, undefined) {
	
	var pybossaEndpoint = '';
	var projectName = '';
	
	function _getProjectId() {
		return $.ajax({
			url : pybossaEndpoint + '/api/project?short_name=' + projectName
		});
	}

	function _getUserProgress() {
		return $.ajax({
			url : pybossaEndpoint + '/api/project/' + projectName + '/userprogress',
			cache : false,
			dataType : 'json'
		});
	}

	function _getAnswers(projectId, userId) {
		return $.ajax({
			url : pybossaEndpoint + '/api/taskrun?project_id=' + projectId
					+ '&user_id=' + userId
		});
	}
	
	pybossaApiHelper.setup = function(endpoint, name) {
		pybossaEndpoint = endpoint;
		projectName = name;
	}
	
	pybossaApiHelper.getProjectId = function() {
		return _getProjectId();
	}
	
	pybossaApiHelper.getUserProgress = function() {
		return _getUserProgress();
	}

	pybossaApiHelper.getAnswers = function(projectId, userId) {
		return _getAnswers(projectId, userId);
	}
	
}(window.pybossaApiHelper = window.pybossaApiHelper || {}, jQuery));
