# -*- coding: utf-8 -*-
from flask import request, make_response
from werkzeug import secure_filename
import pbclient
import pyutil
import requests


class Validador:

    def __init__(self, configuration, template_env):
        self.config = configuration
        self.env = template_env
        self.__setup_pb_client()

    def __setup_pb_client(self):
        pbclient.set('endpoint', self.config['PYBOSSA_ENDPOINT'])
        pbclient.set('api_key', self.config['PYBOSSA_API_KEY'])

    def __find_project(self, app_short_name):
        projects = pbclient.find_project(short_name=app_short_name)
        return projects[0] if len(projects) > 0 else None

    def __setup_project(self, project):
        self.__update_project_info(project)

    def __create_tasks(self, project):
        test_signs = ["ENSINADO", "ENTANTO", "ENTENDIDO"]
        for sign in test_signs:
            video_ref = "/videos/" + sign + "_REF.webm"
            video_ava = "/videos/" + sign + "_AVATAR.webm"
            task = dict(
                sign_name=sign,
                submission_date=pyutil.get_date_now(),
                video_ref=video_ref,
                video_ava=video_ava)
            pbclient.create_task(project.id, task)

    def __update_project_info(self, project):
        template = self.env.get_template('index.html')
        project.info['task_presenter'] = template.render(
            server=self.config['HOST_STATIC_FILES_ENDPOINT'],
            server_backend=self.config['HOST_ENDPOINT'],
            app_shortname=self.config['PYBOSSA_APP_SHORT_NAME'],
            api_db_host=self.config['API_DB_HOST'])
        project.info['thumbnail'] = self.config['HOST_STATIC_FILES_ENDPOINT'] + "/img/thumbnail.png"
        project.info['sched'] = "incremental"
        project.info['published'] = True
        project.allow_anonymous_contributors = False
        pbclient.update_project(project)

    def create_project(self):
        app_short_name = self.config['PYBOSSA_APP_SHORT_NAME']
        project = self.__find_project(app_short_name)
        result_msg = ""
        if (project):
            result_msg = "The project " + app_short_name + " was already created."
        else:
            project = pbclient.create_project(self.config['PYBOSSA_APP_NAME'], app_short_name, self.config['PYBOSSA_APP_DESCRIPTION'])
            if (project):
                self.__setup_project(project)
                result_msg = "The project " + app_short_name + " was created."
            else:
                result_msg = "The project " + app_short_name + " couldn't be created. Check the server log for details."
        pyutil.log(result_msg)
        return result_msg

    def update_project(self):
        app_short_name = self.config['PYBOSSA_APP_SHORT_NAME']
        project = self.__find_project(app_short_name)
        # self.__create_tasks(project)
        self.__update_project_info(project)
        result_msg = "The project " + app_short_name + " was updated."
        pyutil.log(result_msg)
        return result_msg

    def __find_task(self, project_id, task_id):
        tasks = pbclient.find_tasks(project_id, id=task_id)
        return tasks[0] if len(tasks) > 0 else None

    def __find_taskruns(self, project_id, task_id):
        return pbclient.find_taskruns(project_id, task_id=task_id)

    def __number_of_taskruns(self, project_id, task_id):
        taskruns = self.__find_taskruns(project_id, task_id)
        return len(taskruns)

    def __close_task(self, project_id, task_id):
        pyutil.log("Closing the task with ID=" + str(task_id) + ".")
        task = self.__find_task(project_id, task_id)
        number_of_taskruns = self.__number_of_taskruns(project_id, task_id)
        task.n_answers = number_of_taskruns + 1
        pbclient.update_task(task)
        return "The task with ID=" + str(task_id) + " was closed."

    def get_file(self, url, filename):
        r = requests.get(url, stream=True)
        if (r.status_code == 200):
            with open(filename, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
            return True
        return False

    def finish_task(self):
        task_id = request.form['task_id']
        project_id = request.form['project_id']
        sign_name = request.form['sign_name']
        api_dbhost = self.config['API_DB_HOST']
        number_of_approval = int(request.form['number_of_approval'])
        number_of_avatar_disapproval = int(request.form['number_of_avatar_disapproval'])
        number_of_ref_disapproval = int(request.form['number_of_ref_disapproval'])
        # pyutil.log("[VALIDADOR] number_of_approval: %d" % (number_of_approval))
        # pyutil.log("[VALIDADOR] agreement_number: %d" % (agreement_number))
        # pyutil.log("[VALIDADOR] number_of_avatar_disapproval: %d" % (number_of_avatar_disapproval))
        agreement_number = self.config['AGREEMENT_NUMBER']
        result_msg = ""
        code = 200
        selo = None
        if (number_of_approval >= agreement_number):
            selo = 2
        elif (number_of_avatar_disapproval >= agreement_number):
            selo = 4
        elif (number_of_ref_disapproval >= agreement_number):
            selo = 6

        if (selo is None):
            result_msg = "[VALIDADOR] The task with ID=" + str(task_id) + " didn't reach the agreement number yet."
        else:
            body = {
                "nome": sign_name,
                "idtask": task_id,
                "selo": selo
            }
            r = requests.put("%s/updateselo" % (api_dbhost), data=body)
            code = r.status_code
            if (code == 200):
                tipo_selo = dict({
                    2: "especialista",
                    4: "invalido_especialista",
                    6: "invalido_animadores"
                })
                pyutil.log("[VALIDADOR] selo was changed to: %d - %s" % (selo, tipo_selo[selo]))
            else:
                result_msg = r.text
            result_msg = self.__close_task(project_id, task_id)
        pyutil.log(str(result_msg).encode("UTF-8", errors="ignore"))
        return make_response(result_msg, code)
