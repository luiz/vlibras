var Main = function() {

	function _init() {

	}

	function _getSigns() {
		$.ajax({
			type:'GET',
			url: '/api/signs',
			dataType: 'json'
		}).done(function(data) {
			_setTable(data);
		}).fail(function(err) {
			var table = $('.table-wl > tbody');

			var row = 
				'<tr>\
					<td>#</td>\
					<td>Ajax Error</td>\
					<td>Ajax Error</td>\
					<td>Ajax Error</td>\
					<td>Ajax Error</td>\
					<td>Ajax Error</td>\
				<tr>';

			table.append(row);
		});
	}

	function _setTable(data) {
		var table = $('.table-wl > tbody');

		for (var i = 0; i < data.length; i++) {
			var row = 
				'<tr>\
					<td>'+ (i+1) +'</td>\
					<td>'+ data[i].name +'</td>\
					<td>'+ data[i].uuid +'</td>\
					<td>'+ data[i].created_at +'</td>\
					<td>'+ data[i].updated_at +'</td>\
					<td>'+ data[i].status +'</td>\
				</tr>';

			table.append(row);
		}

		_changeColorStatus();
	}

	function _changeColorStatus() {
		var length = $('.table-wl > tbody > tr').length;
		var array = $('.table-wl > tbody > tr > td:last-of-type');

		for (var i = 0; i < length; i++) {
			switch($(array[i]).text()) {
				case 'Sucesso':
					$(array[i]).addClass('_success');
				break;

				case 'Falhou':
					$(array[i]).addClass('_error');
				break;
			}
		}

		_formatDate();
	}

	function _formatDate() {
		var length = $('.table-wl > tbody > tr').length;
		var created_at = $('.table-wl > tbody > tr > td:nth-of-type(4)');
		var updated_at = $('.table-wl > tbody > tr > td:nth-of-type(5)');

		for (var i = 0; i < length; i++) {
			$(created_at[i]).text($.format.date($(created_at[i]).text(), "HH:mm:ss - dd/MM/yyyy"));
			$(updated_at[i]).text($.format.date($(updated_at[i]).text(), "HH:mm:ss - dd/MM/yyyy"));
		}
	}

	return {
		run: function() {
			_init();
			_getSigns();
		}
	}
};

var main = new Main();
main.run();