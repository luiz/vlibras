function read_all(Sign, callback) {
	Sign.find(function(err, signs) {
	  	if (err) callback(null);

	  	callback(signs);
	});
};

function create(object, callback) {
	object.save(function(err, sign) {
		if (err) callback(null);
		
		callback(sign);
	});
};

function remove(Sign, hash, callback) {
	Sign.remove({ uuid: hash }, function(err, sign) {
		if (err) callback(null);

		callback(sign);
	});
};

module.exports.read_all = read_all;
module.exports.create = create;
module.exports.remove = remove;