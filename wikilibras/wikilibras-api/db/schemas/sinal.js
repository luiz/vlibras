function init_schema(mongoose) {
 	var signSchema = new mongoose.Schema({
	  	name: String,
	  	uuid: String,
	  	status: String,
	  	created_at: { type: Date },
	  	updated_at: { type: Date }
	});

	var Sign = mongoose.model('Sign', signSchema);

	return Sign;
};

module.exports.init = init_schema;