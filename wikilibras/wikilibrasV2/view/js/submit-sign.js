(function(submitSign, $, undefined) {

	var submitUrl = '';
	var loggedUser = '';
	var MAX_CHAR_LENGTH = 255;

	function _isEmpty(str) {
		return (!str || 0 === str.length);
	}

	function _alertSignName() {
		$('#upload-warning-msg').hide();
		var signName = $('#input-sign-name').val();
		if (_isEmpty(signName)) {
			$('#upload-warning-msg').html('Por favor indique o nome do sinal.');
			$('#upload-warning-msg').show();
		}
	}

	function _alertWordClass() {
		$('#upload-warning-msg').hide();
		var wordClass = $('#input-word-class').val();
		if (_isEmpty(wordClass)) {
			$('#upload-warning-msg').html(
					'Por favor selecione a classe gramatical do sinal.');
			$('#upload-warning-msg').show();
		}
	}

	function _arePhrasesValid(phrases) {
		return phrases.length <= MAX_CHAR_LENGTH;
	}

	function _alertPhrase() {
		$('#upload-warning-msg').hide();
		var phrases = $('#input-phrases').val();
		if (!_arePhrasesValid(phrases)) {
			$('#upload-warning-msg').html(
					'As frases de exemplo devem possuir no máximo '
							+ MAX_CHAR_LENGTH + ' caracteres.');
			$('#upload-warning-msg').show();
		}
	}

	function _alertState() {
		$('#upload-warning-msg').hide();
		var state = $('#input-state').val();
		if (_isEmpty(state)) {
			$('#upload-warning-msg').html('Por favor selecione seu estado.');
			$('#upload-warning-msg').show();
		}
	}

	function _alertSignUpload() {
		$('#upload-warning-msg').hide();
		var signUpload = $('#input-sign-upload').val();
		if (_isEmpty(signUpload)) {
			$('#upload-warning-msg').html(
					'Por favor selecione o arquivo do vídeo do seu sinal.');
			$('#upload-warning-msg').show();
		}
	}

	function _getNationalRadioCheckedValue() {
		return $(".wl-national-sign-radio:checked").attr("value");
	}

	function _isValidState(state) {
		var value = _getNationalRadioCheckedValue();
		return value === 'no' && state != '' || value === 'yes';
	}

	function _validadeAllFields() {
		var signName = $('#input-sign-name').val();
		var wordClass = $('#input-word-class').val();
		var phrases = $('#input-phrases').val();
		var state = $('#input-state').val();
		var signUpload = $('#input-sign-upload').val();
		return !_isEmpty(signName) && !_isEmpty(wordClass)
				&& _arePhrasesValid(phrases) && !_isEmpty(signUpload)
					&& _isValidState(state);
	}

	function _updateSubmitButton() {
		if (_validadeAllFields()) {
			_enableSubmitButton();
		} else {
			_disableSubmitButton();
		}
	}

	function _enableSubmitButton() {
		$('#submit-sign-container button').removeClass('disabled');
	}

	function _disableSubmitButton() {
		$('#submit-sign-container button').addClass('disabled');
	}

	function _resetFormFields() {
		$('#input-sign-name').val('');
		$('#input-word-class').val('');
		$('#input-phrases').val('');
		$('#input-state').val('');
		$('#input-city').val('');
		$('#input-sign-upload').val('');
		$("#upload-progress .progress-bar").css("width", "0px");
		$('#upload-progress-container').hide();
		$('#input-sign-upload').show();
	}

	function _setupSubmitSignForm() {
		_disableSubmitButton();
		$('#input-user-login').attr('value', loggedUser);
		$('#submit-sign-container form').fileupload(
				{
					url : submitUrl,
					add : function(e, data) {
						$('#submit-sign-container button').off('click').on(
								'click',
								function(event) {
									event.preventDefault();
									if ($(this).hasClass('disabled')) return;

									$('#submit-sign-container button')
											.addClass('disabled');
									$('#input-sign-upload').hide();
									$('#upload-progress-container').show();
									data.submit();
								});
					},
					done : function(e, data) {
						$('#upload-success-msg').fadeIn(500);
						_resetFormFields();
						setTimeout(function() {
							$('#upload-success-msg').fadeOut(500);
						}, 5000);
					},
					progressall : function(e, data) {
						var progress = parseInt(data.loaded / data.total * 100,
								10);
						$("#upload-progress .progress-bar").attr(
								'aria-valuenow', progress).css("width",
								progress + "%");
					},
					error : function(error) {
						alert(error.responseText);
						_enableSubmitButton();
					},
					replaceFileInput : false
				});

		$('#input-sign-name').on('input', function() {
			_alertSignName();
			_updateSubmitButton();
		});
		$('#input-word-class').on('input', function() {
			_alertWordClass();
			_updateSubmitButton();
		});
		$('#input-phrases').on('input', function() {
			_alertPhrase();
			_updateSubmitButton();
		});
		$('#input-sign-upload').on('change', function() {
			_alertSignUpload();
			_updateSubmitButton();
		});

		$('#input-state').on('input', function() {
			var value = $(this).val();
			if (value === 'Nacional') {
				$('.wl-national-sign-radio[value=yes]').trigger("click");
			} else {
				_alertState();
			}

			_updateSubmitButton();
		});
		$('.wl-national-sign-radio').on('click', function() {
			var value = $(this).attr("value");
			if (value === 'no') {
				$("#input-state-container").show();
				$("#input-city-container").show();
				$("#input-state").val('');
			} else {
				$("#input-state-container").hide();
				$("#input-city-container").hide();
				$("#input-state").val('Nacional');
				$("#input-city").val('');
			}

			_updateSubmitButton();
		});
	}

	submitSign.show = function() {
		$(".sub-main-container").hide();
		$("#submit-sign-container").show();
		$("#submit-sign-anchor").focus();
	};

	submitSign.setup = function(uploadSignHost, user) {
		submitUrl = uploadSignHost + "/addsinal";
		loggedUser = user;
		loadHtmlHelper.load('/submit-sign/submit-sign.html',
				'#submit-sign-container', _setupSubmitSignForm);
	};

}(window.submitSign = window.submitSign || {}, jQuery));