(function(ranking, $, undefined) {

	var NUMBER_OF_TOP_USERS = 10;
	var STARS_MAP = ['', 'gold', 'silver', 'bronze'];
	var baseUrl = '';
	var pybossaEndpoint = '';
	var projectName = '';
	var loggedUser = {};
	var totalTasks = 0;
	var doneTasks = 0;

	function _getRankingData(callback) {
		$.ajax({
			url : pybossaEndpoint + '/api/leaderboard?limit='
					+ NUMBER_OF_TOP_USERS,
			success : function(response) {
				callback(typeof response == 'object' ? response[projectName]
						: '');
			},
			error : function(xhr, textStatus, error) {
				alert(xhr.responseText);
			}
		});
	}

	function _processRankingRow(rowData) {
		var rank = rowData.rank;
		if (rank < 0) {
			return '';
		}
		var starHTML = '';
		if (rank > 0 && rank < 4) {
			starHTML = '<img src="' + baseUrl + '/img/ranking/'
					+ STARS_MAP[rank] + '-star-icon.png">';
		}
		var trHTML = '<tr>';
		if (rowData.name === loggedUser.name) {
			trHTML = '<tr class="warning">';
			loggedUser.rank = rowData.rank;
		}
		rank = rank == 0 ? '-' : rank + '.';
		return trHTML + '<td>' + starHTML + '</td><td>' + rank
				+ '</td><td><a href="/pybossa/account/' + rowData.name
				+ '/" target="_blank">' + rowData.fullname + '</a></td><td>'
				+ rowData.score + '</td></tr>';
	}

	function _updateRanking() {
		_getRankingData(function(data) {
			if (data === '')
				return;

			var rowsHTML = '';
			for (var i = 0; i < data.length; i++) {
				rowsHTML += _processRankingRow(data[i]);
			}
			$('#leaderboard-container tbody').html(rowsHTML);
			$('#ranking-info-container .rank-position').html(loggedUser.rank);
			$('#ranking-info-container .username').html(loggedUser.fullName);
			if (loggedUser.avatarUrl != '') {
				$('#ranking-info-container .avatar-container img').attr('src',
						loggedUser.avatarUrl);
				$('#ranking-info-container .avatar-placeholder').hide();
				$('#ranking-info-container .avatar-container').show();
			}
			if (loggedUser.rank === 0) {
				$('#ranking-info-container .rank-position-container').hide();
			}
			_updateProgress();
		});
	}

	function _getUserData() {
		return $.ajax({
			url : pybossaEndpoint + '/api/user?name=' + loggedUser.name
		});
	}

	function _getAvatarUrl(data) {
		return !data || typeof data.avatar === 'undefined' || typeof data.container === 'undefined' ?
				'' : pybossaEndpoint + '/uploads/' + data.container + '/' + data.avatar;
	}

	function _updateProgress() {
		pybossaApiHelper
				.getUserProgress()
				.done(
						function(response) {
							totalTasks = response.total;
							doneTasks = response.done;
							var percentage = (doneTasks / totalTasks) * 100;
							$('#ranking-info-container .progress-bar').attr(
									'aria-valuenow', percentage).css('width',
									percentage + '%');
							$('#ranking-container [data-toggle="tooltip"]')
									.tooltip(
											{
												title : '<strong><span class="done-tasks">'
														+ doneTasks
														+ '</span> / <span class="total-tasks">'
														+ totalTasks
														+ '</span> sinais ensinados.</strong>',
												placement : 'bottom',
												trigger : 'manual'
											});
						});
	}

	function _loadRankingData() {
		_getUserData().done(function(response) {
			if (typeof response == 'undefined' || response.length < 1) {
				return;
			}
			loggedUser.fullName = response[0].fullname;
			loggedUser.avatarUrl = _getAvatarUrl(response[0].info);
			_updateRanking();
		});
	}

	ranking.show = function() {
		$('.sub-main-container').hide();
		$('#ranking-container').show();

		if (doneTasks > 0) {
			$('#ranking-container [data-toggle="tooltip"]').tooltip('show');
		}
	}

	ranking.setup = function(serverUrl, endpoint, name, user) {
		baseUrl = serverUrl;
		pybossaEndpoint = endpoint;
		projectName = name;
		loggedUser.name = user;
		loadHtmlHelper.load('/ranking/ranking.html', '#ranking-container',
				_loadRankingData);
	};

}(window.ranking = window.ranking || {}, jQuery));