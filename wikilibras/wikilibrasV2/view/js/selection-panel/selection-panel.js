(function(selectionPanel, $, undefined) {
	
	function _selectAnOption(parentId, el) {
		$(parentId + ' .selection-panel-option[select=true]').removeAttr(
				'select');
		$(el).attr('select', true);

		var mainConfig = $(parentId).attr('mainConfig');
		var subConfig = $(parentId).attr('subConfig');
		var step = $(parentId).attr('step');
		wikilibras.updateTempParameterJSON(mainConfig, subConfig, step, $(el).attr(
				'value'));
	}
	
	function _canRenderSignVideo() {
		return _isConfigurationComplete('facial') &&
				(_isConfigurationComplete('right-hand') || _isConfigurationComplete('left-hand'));
	}
	
	function _isConfigurationComplete(config) {
		var baseId = '.subconfiguration-panel[mainConfig=' + config + ']';
		var total_config = $(baseId
				+ ' .icon_container[json_name][active=true]').length;
		var completed_config = $(baseId
				+ ' .icon_container[active=true][complete=true]').length;
		return completed_config != 0 && total_config == completed_config;
	}
	
	function _clearPreviousSelection() {
		$('.selection-panel-body').hide();
		$('.subconfiguration-panel').hide();

		if (configurationScreen.isMenuSelected()) {
			var current_option = configurationScreen.getCurrentMainConfiguration();
			iconHelper.selectIcon(current_option, false);
			if (_isConfigurationComplete(current_option)) {
				iconHelper.setupCheckIcon(current_option, true);
			}
			$('#avatar-' + current_option).fadeOut(500);
		}
	}
	
	function _finishConfiguration(config, toFinish) {
		iconHelper.setupCheckIcon(config, toFinish);
		iconHelper.setupCheckIcon('avatar-' + config, toFinish);

		if (toFinish) {
			$('#' + config + '-edit .check-icon').show();
		} else {
			$('#' + config + '-edit .check-icon').hide();
		}
		if (_canRenderSignVideo()) {
			$('#ready-button').removeClass('disabled');
		} else {
			$('#ready-button').addClass('disabled');
		}
	}
	
	function _unfinishConfiguration(config, panel) {
		iconHelper.setupCheckIcon(config, false, panel);
		iconHelper.setupCheckIcon('avatar-' + config, false, panel);
		$('#' + config + '-edit .check-icon').hide();

		if (!_canRenderSignVideo()) {
			$('#ready-button').addClass('disabled');
		}
	}
	
	function _addZoomInToAvatar(option, callback) {
		$('#avatar-default')
				.fadeOut(
						500,
						function() {
							$('#avatar-container').removeClass('col-sm-7');
							$('#avatar-container').addClass('col-sm-5');
							$('#selection-container').removeClass('col-sm-2');
							$('#selection-container').addClass('col-sm-4');
							$('#avatar-container').removeClass(
									'avatar-container-zoom-out');
							$('#avatar-container').addClass(
									'avatar-container-zoom-in');
							$('#avatar-' + option).removeClass(
									'avatar-img-zoom-out');
							$('#avatar-' + option).fadeIn(
									500,
									function() {
										$('#avatar-' + option).addClass(
												'avatar-' + option
														+ '-img-zoom-in');
										callback();
									});
						});
	}

	function _addZoomOutToAvatar(option, callback) {
		$('#avatar-' + option).fadeOut(
				500,
				function() {
					$('#selection-container').removeClass('col-sm-4');
					$('#selection-container').addClass('col-sm-2');
					$('#avatar-container').removeClass('col-sm-5');
					$('#avatar-container').addClass('col-sm-7');
					$('#avatar-container').removeClass(
							'avatar-container-zoom-in');
					$('#avatar-container')
							.addClass('avatar-container-zoom-out');
					$('#avatar-default').fadeIn(
							500,
							function() {
								$('#avatar-' + option).removeClass(
										'avatar-' + option + '-img-zoom-in');
								$('#avatar-' + option).addClass(
										'avatar-img-zoom-out');
								callback();
							});
				});
	}
	
	function _hide() {
		var config = configurationScreen.getCurrentMainConfiguration();
		if (config === '') return;
		
		iconHelper.deselectIcon(config);
		if (_isConfigurationComplete(config)) {
			_finishConfiguration(config, true);
		} else {
			_finishConfiguration(config, false);
		}

		_addZoomOutToAvatar(config, function() {
			$('#ready-button').fadeIn(300);
			$('.edit-container').fadeIn(300);
		});
		$('#selection-panel').fadeOut(300);
	}
	
	function _setupGUIOnSelection(option, finishCallback) {
		$('#ready-button').fadeOut(300);
		$('.edit-container').fadeOut(300);
		_addZoomInToAvatar(option, function() {
			$('#selection-panel').fadeIn(300, function() {
				finishCallback();
			});
		});
	}
	
	function _show(option) {
		_clearPreviousSelection();
		iconHelper.selectIcon(option, true);
		dynworkflow.selectMainConfig(option);
		_setupGUIOnSelection(option, function() {
			dynworkflow.initTimeline();
		});
	}
	
	selectionPanel.selectAnOption = function (parentId, el) {
		_selectAnOption(parentId, el);
	}
	
	selectionPanel.unfinishConfiguration = function(config, panel) {
		return _unfinishConfiguration(config, panel);
	}
	
	selectionPanel.isConfigurationComplete = function(config) {
		return _isConfigurationComplete(config);
	}
	
	selectionPanel.hide = function() {
		return _hide();
	}
	
	selectionPanel.show = function(option) {
		_show(option);
	}
	
	selectionPanel.clean = function() {
		articulation.clean();
		$(".selection-panel-option").removeAttr('select');
		$(".icon_container").removeAttr("select");
		$(".icon_container[complete=true]").each(
				function() {
					_unfinishConfiguration($(this).attr("name"), $(this).attr(
							"panel"));
		});
	}
	
	selectionPanel.setup = function(url) {
		$('#selection-panel .x').off('click').on('click', function() {
			_hide();
		});
		selectionPanel.clean();
	};	

}(window.selectionPanel = window.selectionPanel || {}, jQuery));
