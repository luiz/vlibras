(function(dynworkflow, $, undefined) {

	// Workflow configuration
	var jsonWF = {};
	var baseUrl = '';

	// Main configurations: right-hand, left-hand and facial
	var mainConfig = '';
	// The converted Main Config (right/left-hand) to hand for using the same configuration
	var preprocessedMainConfig = '';
	// Subconfigurations: movimento, articulacao, configuracao, orientacao, etc
	var currentSubconfig = '';
	var currentSubConfigName = '';
	var currentSubconfigParent = '';
	var currentStep = 0;

	function _preprocessMainConfig(config) {
		config = config.replace('right-hand', 'hand');
		config = config.replace('left-hand', 'hand');
		return config;
	}

	function _getFirstKey(json) {
		var first_key = undefined;
		for (first_key in json)
			break;
		return first_key;
	}

	function _getAttributes(json) {
		var result = [];
		for (attr in json) {
			result.push(attr);
		}
		return result;
	}

	function _updateAndGetFirstMovementSubConfig() {
		var selectedMovement = movement.getPreviousSelectedMovement(mainConfig);
		if (typeof selectedMovement == 'undefined')
			return -1;

		currentSubconfigParent = jsonWF[preprocessedMainConfig]['movimento'][selectedMovement];
		currentSubConfigName = _getFirstKey(currentSubconfigParent);
		return currentSubConfigName;
	}

	function _updateAndGetMovementConfig() {
		currentSubconfigParent = jsonWF[preprocessedMainConfig];
		currentSubConfigName = _getFirstKey(currentSubconfigParent);
		return currentSubConfigName;
	}

	function _getNextSubConfig(toForward) {
		var attrs = _getAttributes(currentSubconfigParent);
		for (var i = 0; i < attrs.length; i++) {
			if (toForward && attrs[i] == currentSubConfigName
					&& i < attrs.length - 1) {
				return attrs[i + 1];
			} else if (!toForward && attrs[i] == currentSubConfigName && i >= 1) {
				return attrs[i - 1];
			}
		}
		if (toForward && currentSubConfigName == 'movimento') {
			return _updateAndGetFirstMovementSubConfig();
		} else if (!toForward && preprocessedMainConfig == 'hand') {
			return _updateAndGetMovementConfig();
		} else if (!toForward) {
			return currentSubConfigName;
		} else {
			return -1;
		}
	}

	function _showCurrentSubconfig() {
		_showSubconfiguration(mainConfig, currentSubConfigName, currentStep);
	}

	// It checks if a selection panel is already loaded
	function _isSubconfigurationPanelLoaded(mainConfig, subConfig, stepNumber) {
		var stepNumber = stepNumber + 1;
		return $('.selection-panel-body[mainConfig=' + mainConfig
				+ '][subConfig=' + subConfig + '][step=' + stepNumber + ']').length > 0;
	}

	function _showLoadedSubconfigurationPanel(mainConfig, subConfig, stepNumber) {
		var stepNumber = stepNumber + 1;
		return $(
				'.selection-panel-body[mainConfig=' + mainConfig
						+ '][subConfig=' + subConfig + '][step=' + stepNumber
						+ ']').show();
	}

	// It renders or shows the requested selection panel
	function _showSubconfiguration(mainConfig, subConfig, stepNumber) {
		$('.selection-panel-body').hide();
		if (_isSubconfigurationPanelLoaded(mainConfig, subConfig, stepNumber)) {
			_showLoadedSubconfigurationPanel(mainConfig, subConfig, stepNumber);
		} else {
			var step = currentSubconfig[stepNumber];
			step = typeof step == 'undefined' ? 'passo-1' : step;
			loadHtmlHelper.append('/' + preprocessedMainConfig + '/'
					+ subConfig + '/' + step + '.html', '#selection-panel',
					true);
		}
		_selectTimelineIcon(mainConfig, subConfig, true);
	}

	function _selectSubConfig(subConfig) {
		if (subConfig == 'movimento') {
			_updateAndGetMovementConfig();
		} else if (currentSubConfigName == 'movimento') {
			_updateAndGetFirstMovementSubConfig();
		}
		currentSubConfigName = subConfig;
		currentSubconfig = currentSubconfigParent[currentSubConfigName];
		currentStep = 0;
		_showCurrentSubconfig();
	}

	// It shows the next selection panel on the workflow
	function _showNextSubConfig() {
		_walkOnTheWorkflow(true);
	}

	function _showPreviousSubConfig() {
		_walkOnTheWorkflow(false);
	}

	function _walkOnTheWorkflow(toForward) {
		currentStep = toForward ? currentStep + 1 : currentStep - 1;

		if (currentStep >= 0 && currentStep < currentSubconfig.length) {
			_showCurrentSubconfig();
		} else {
			var nextSubConfig = _getNextSubConfig(toForward);
			if (nextSubConfig != -1) {
				_selectSubConfig(nextSubConfig);
			} else {
				selectionPanel.hide();
			}
		}
	}

	function _checkIfFinished(mainConfig, currentSubConfigName) {
		var numberOfSteps = currentSubconfig.length;
		var completedSteps = $('.selection-panel-body[mainConfig=' + mainConfig
				+ '][subConfig=' + currentSubConfigName
				+ '] .selection-panel-option[select=true]').length;
		return completedSteps != 0 && completedSteps == numberOfSteps;
	}

	// A callback function to be called when the user selects a option on a panel
	function _userSelectedAnOption() {
		if (_checkIfFinished(mainConfig, currentSubConfigName)) {
			_setupCheckIcon(mainConfig, currentSubConfigName);
		}
		_showNextSubConfig();
	}

	function _cleanStep(mainConfig, subConfig, step) {
		var baseId = '.selection-panel-body[mainConfig=' + mainConfig
				+ '][subConfig=' + subConfig + '][step=' + step + ']';
		$(baseId + ' .selection-panel-option').removeAttr('select');
		var icon_id = '.subconfiguration-panel[mainConfig=' + mainConfig
				+ '] .icon_container[json_name=' + subConfig + ']';
		$(icon_id).removeAttr('complete');
	}

	// Timeline functions
	function _selectTimelineIcon(mainConfig, subConfig) {
		var baseId = '.subconfiguration-panel[mainConfig=' + mainConfig
				+ '] .subconfiguration-options';
		var iconContainer = '.icon_container[json_name=' + subConfig + ']';
		var iconId = baseId + ' ' + iconContainer;

		var previousSelected = $(baseId + ' .icon_container[select=true]')
				.attr('json_name');
		if (typeof previousSelected != 'undefined') {
			_deselectTimelineIcon(mainConfig, previousSelected);
		}

		iconHelper.enableIconHover($(iconId), true);
		$(iconId).attr('select', true);
		$(baseId).scrollTo(iconContainer, {
			'offset' : -60,
			'duration' : 750
		});
	}

	function _deselectTimelineIcon(mainConfig, subConfig) {
		var icon_id = '.subconfiguration-panel[mainConfig=' + mainConfig
				+ '] .icon_container[json_name=' + subConfig + ']';

		if ($(icon_id + '[complete=true]').length > 0) {
			_setupCheckIcon(mainConfig, subConfig);
		} else {
			iconHelper.enableIconHover($(icon_id), false);
			$(icon_id).removeAttr('select');
		}
	}

	function _setupCheckIcon(mainConfig, subConfig) {
		var icon_id = $('.subconfiguration-panel[mainConfig=' + mainConfig
				+ '] .icon_container[json_name=' + subConfig + ']');
		iconHelper.enableIconCheck(icon_id, true);
		$(icon_id).attr('complete', true);
		$(icon_id).attr('select', false);
	}

	function _isTimelineLoaded() {
		return $('.subconfiguration-panel[mainConfig=' + mainConfig + ']').length > 0;
	}

	function _setupTimelineListeners(timelineBaseId) {
		$(timelineBaseId + ' .icon_container[json_name]').off('click').on(
				'click', function() {
					var subConfig = $(this).attr('json_name');
					_selectSubConfig(subConfig);
				});
		$(timelineBaseId + ' .icon_container[json_name]').off('mouseover').on(
				'mouseover', function() {
					if (iconHelper.canHover(this)) {
						iconHelper.enableIconHover(this, true);
					}
				});
		$(timelineBaseId + ' .icon_container[json_name]').off('mouseout').on(
				'mouseout', function() {
					if (iconHelper.canHover(this)) {
						iconHelper.enableIconHover(this, false);
					}
				});
		$(timelineBaseId + ' .arrow[name=right-arrow]').off('click').on(
				'click', function() {
					_showNextSubConfig();
				});
		$(timelineBaseId + ' .arrow[name=left-arrow]').off('click').on('click',
				function() {
					_showPreviousSubConfig();
				});
	}

	function _setupTimelineIcons(timelineBaseId, toUpdate) {
		if (!toUpdate) {
			$(timelineBaseId).show();
			$(timelineBaseId + " .subconfiguration-options").scrollTo(0, 0);
			return;
		}

		$(timelineBaseId + ' .icon_container[json_name]').attr("active",
				"false");
		for ( var name in currentSubconfigParent) {
			$(timelineBaseId + ' .icon_container[json_name=' + name + ']')
					.attr("active", "true");
		}

		if (preprocessedMainConfig == 'hand') {
			$(timelineBaseId + ' .icon_container[json_name=movimento]').attr(
					"active", "true");
			_setupCheckIcon(mainConfig, 'movimento');
		}
		_selectTimelineIcon(mainConfig, currentSubConfigName);
		_setupTimelineListeners(timelineBaseId);
		$(timelineBaseId).show();
	}

	function _setupTimeline(toUpdate) {
		var timelineBaseId = '.subconfiguration-panel[mainConfig=' + mainConfig
				+ ']';
		if (_isTimelineLoaded()) {
			_setupTimelineIcons(timelineBaseId, toUpdate);
		} else {
			loadHtmlHelper.append('/' + preprocessedMainConfig
					+ '/timeline.html', '#selection-panel', false, function() {
				_setupTimelineIcons(timelineBaseId, true);
			});
		}
	}

	function _initTimeline() {
		if (preprocessedMainConfig != 'hand' || _isTimelineLoaded()) {
			_setupTimeline(false);
		}
	}

	function _cleanTimeline() {
		$(".subconfiguration-panel").remove();
	}
	
	function _cleanPreviousLoadedPanel() {
		$('.selection-panel-body[mainConfig=' + mainConfig + ']').each(
				function() {
					var subConfigName = $(this).attr("subConfig");
					if (subConfigName.indexOf("articulacao") != -1
							|| subConfigName.indexOf("configuracao") != -1
							|| subConfigName.indexOf("orientacao") != -1
							|| subConfigName.indexOf("movimento") != -1) {
						return;
					}
					$(
							'.selection-panel-body[mainConfig=' + mainConfig
									+ '][subConfig=' + subConfigName + ']')
							.remove();
				});
	}

	// Public methods
	dynworkflow.selectMainConfig = function(config) {
		mainConfig = config;
		preprocessedMainConfig = _preprocessMainConfig(mainConfig);
		currentSubconfigParent = jsonWF[preprocessedMainConfig];
		currentSubConfigName = _getFirstKey(currentSubconfigParent);
		currentSubconfig = currentSubconfigParent[currentSubConfigName];
		currentStep = 0;

		_showCurrentSubconfig();
	};

	dynworkflow.selectMovement = function(movement) {
		var subconfigJSON = currentSubconfig[movement];
		currentSubConfigName = _getFirstKey(subconfigJSON);
		currentSubconfigParent = subconfigJSON;
		currentSubconfig = subconfigJSON[currentSubConfigName];
		currentStep = 0;

		_cleanPreviousLoadedPanel();
		_showCurrentSubconfig();
		_setupTimeline(true);
	};

	dynworkflow.selectSubConfig = function(subConfig) {
		_selectSubConfig(subConfig);
	};

	dynworkflow.userSelectedAnOption = function() {
		_userSelectedAnOption();
	};

	dynworkflow.cleanStep = function(mainConfig, subConfig, step) {
		_cleanStep(mainConfig, subConfig, step);
	};

	dynworkflow.getFacialParameters = function() {
		return _getAttributes(jsonWF['facial']);
	};

	dynworkflow.getMovementParameters = function(movementName) {
		return _getAttributes(jsonWF['hand']['movimento'][movementName]);
	};

	dynworkflow.getMainConfig = function() {
		return mainConfig;
	};
	
	dynworkflow.initTimeline = function() {
		_initTimeline();
	};
	
	dynworkflow.setup = function(url) {
		baseUrl = url;
		$.get(baseUrl + '/conf/selection-workflow-json', function(result) {
			jsonWF = $.parseJSON(result);
		}).fail(function() {
			console.log('Failed to load the workflow configuration');
		});
		_cleanTimeline();
	};

}(window.dynworkflow = window.dynworkflow || {}, jQuery));
