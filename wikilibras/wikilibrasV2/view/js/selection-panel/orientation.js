(function(orientation, $, undefined) {

  orientation.setup = function(hand, subConfig, step) {
    var baseId = '.selection-panel-body[mainConfig=' + hand + '][subConfig=' +
			subConfig + '][step=' + step + ']';
    $(baseId + ' .selection-panel-option').off('click').on(
        'click', function() {
          selectionPanel.selectAnOption(baseId, this);
          dynworkflow.userSelectedAnOption();
        });
  };

}(window.orientation = window.orientation || {}, jQuery));
