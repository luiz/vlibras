(function(tmpJSONParser, $, undefined) {

  var base_parameter_json = {};
  var movement_parameter_json = {};

  function _setupBaseParameterJSON(tmpJSON) {
    base_parameter_json['userId'] = tmpJSON['userId'];
    base_parameter_json['sinal'] = tmpJSON['sinal'];
    base_parameter_json['interpolacao'] = 'normal';
    base_parameter_json['movimentos'] = [];
    movement_parameter_json = {
      'facial': {},
      'mao_direita': {},
      'mao_esquerda': {}
    };
    base_parameter_json['movimentos'].push(movement_parameter_json);
  }

  function _parseParameterValue(value) {
	if (typeof value == 'string' && value.toLowerCase() == 'true') {
		return true;
	} else if (typeof value == 'string' && value.toLowerCase() == 'false') {
		return false;
	} else {
		return !isNaN(value) ? parseInt(value) : value;
	}
  }

  function _parseTempFacialParameterJSON(tmpJSON) {
    var attrs = dynworkflow.getFacialParameters();
    for (var i in attrs) {
      var attr = attrs[i];
      parameterValue = tmpJSON['facial'][attr][0];
      movement_parameter_json['facial'][attr] = _parseParameterValue(parameterValue);
    }
  }
  
  function _parseHand(hand) {
	var parsedHand = hand == 'right-hand' ? 'mao_direita' : hand;
	parsedHand = hand == 'left-hand' ? 'mao_esquerda' : parsedHand;
	return parsedHand;
  }
  
  // Default parser
  function _defaultMovementParser(tmpJSON, movementName, hand) {
	var attrs = dynworkflow.getMovementParameters(movementName);
	var parsedHand = _parseHand(hand);
	
	for (var i in attrs) {
	    var attr = attrs[i];
	    var parameterValue = '';
	    if (typeof tmpJSON[hand][attr] == "undefined") {
	    	continue;
	    }
	    if (attr == 'configuracao') {
	      parameterValue = tmpJSON[hand][attr][1];
	    } else if (attr == 'articulacao') {
	      parameterValue = articulation.processValue(hand, tmpJSON[hand][attr]);
	    } else {
	      parameterValue = tmpJSON[hand][attr][0];
	    }
	    movement_parameter_json[parsedHand][movementName][attr] =
	      _parseParameterValue(parameterValue);
	}
  }
	
  function _retilinearMovementParser(tmpJSON, movementName, hand) {
	var attrs = dynworkflow.getMovementParameters(movementName);
	var parsedHand = _parseHand(hand);
	
	for (var i in attrs) {
	    var attr = attrs[i];
	    var initParameterValue = '';
	    var endParameterValue = '';
	    if (attr == 'configuracao-retilineo') {
	      initParameterValue = tmpJSON[hand][attr][1];
	      endParameterValue = tmpJSON[hand][attr][3];
	    } else if (attr == 'articulacao-retilineo') {
	      initSlice = tmpJSON[hand][attr].slice(0, 2);
	      endSlice = tmpJSON[hand][attr].slice(2, 4);
	      initParameterValue =  articulation.processValue(hand, initSlice);
	      endParameterValue =  articulation.processValue(hand, endSlice);
	    } else {
	      initParameterValue = tmpJSON[hand][attr][0];
	      endParameterValue = tmpJSON[hand][attr][1];
	    }
	    attr = attr.replace('-retilineo', '');
	    var initAttr = attr + '_inicial';
	    var endAttr = attr + '_final';
	    movement_parameter_json[parsedHand][movementName][initAttr] =
	      _parseParameterValue(initParameterValue);
	    movement_parameter_json[parsedHand][movementName][endAttr] =
		      _parseParameterValue(endParameterValue);
	}
  }
  
  function _parseTempMovementParameterJSON(tmpJSON, hand) {
    var movimentConfig = tmpJSON[hand]['movimento'];
    if (typeof movimentConfig == 'undefined') return;

    var movementName = movimentConfig[0];
    var parsedHand = _parseHand(hand);
    movement_parameter_json[parsedHand][movementName] = {};
    
    if (movementName == 'retilineo') {
    	_retilinearMovementParser(tmpJSON, movementName, hand);
    } else {
    	_defaultMovementParser(tmpJSON, movementName, hand);
    }
  }

  tmpJSONParser.parse = function(tmpJSON, rightHand, leftHand) {
    _setupBaseParameterJSON(tmpJSON);
    _parseTempFacialParameterJSON(tmpJSON);
    if (rightHand) {
        _parseTempMovementParameterJSON(tmpJSON, 'right-hand');
    }
    if (leftHand) {
    	_parseTempMovementParameterJSON(tmpJSON, 'left-hand');	
    }    
    return base_parameter_json;
  };

}(window.tmpJSONParser = window.tmpJSONParser || {}, jQuery));
