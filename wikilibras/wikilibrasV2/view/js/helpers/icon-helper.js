(function(iconHelper, $, undefined) {
	
	var baseUrl = '';
	
	function _changeImage(img, url) {
		img.attr('src', url);
	}
	
	function _enableIconHover(container, isHover) {
		var img = $(container).find('img').first();
		var hover_img_url = baseUrl + '/img/' + $(container).attr('name');
		if (isHover) {
			hover_img_url += '-icon-hover.png';
		} else {
			hover_img_url += '-icon.png';
		}
		_changeImage(img, hover_img_url);
	}
	
	function _enableIconCheck(container, isCheck) {
		var img = $(container).find('img').first();
		var check_img_url = baseUrl + '/img/' + $(container).attr('name');
		if (isCheck) {
			check_img_url += '-icon-check.png';
		} else {
			check_img_url += '-icon.png';
		}
		_changeImage(img, check_img_url);	
	}

	function _selectIcon(iconName, isSelect, panel) {
		panel = typeof panel == 'undefined' ? '' : '[panel=' + panel + ']';
		var icon_id = '.icon_container[name=' + iconName + ']' + panel;
		_enableIconHover(icon_id, isSelect);
		$(icon_id).attr('select', isSelect);
	}
	
	function _deselectIcon(iconName, parent) {
		_selectIcon(iconName, false, parent);
	}
	
	function _setupCheckIcon(option, isCheck, panel) {
		panel = typeof panel == 'undefined' ? '' : '[panel=' + panel + ']';
		var icon_id = '.icon_container[name=' + option + ']' + panel;
		iconHelper.enableIconCheck(icon_id, isCheck);
		$('.icon_container[name=' + option + ']' + panel).attr('complete',
				isCheck);
	}
	
	function _canHover(el) {
		var incompleteConfig = typeof $(el).attr('complete') == 'undefined'
				|| $(el).attr('complete') == 'false';
		return (!configurationScreen.isMenuSelected() && incompleteConfig)
				|| (typeof $(el).attr('select') == 'undefined' && incompleteConfig);
	}

	iconHelper.enableIconHover = function(container, isHover) {
		_enableIconHover(container, isHover);
	}
	
	iconHelper.enableIconCheck = function(container, isCheck) {
		_enableIconCheck(container, isCheck);
	}
	
	iconHelper.setupCheckIcon = function(option, isCheck, panel) {
		_setupCheckIcon(option, isCheck, panel);
	}
	
	iconHelper.selectIcon = function(iconName, isSelect, panel) {
		_selectIcon(iconName, isSelect, panel);
	}
	
	iconHelper.deselectIcon = function(iconName, parent) {
		_deselectIcon(iconName, parent);
	}
	
	iconHelper.canHover = function(el) {
		return _canHover(el);
	}
	
	iconHelper.setup = function(url) {
		baseUrl = url;
	};

}(window.iconHelper = window.iconHelper || {}, jQuery));
