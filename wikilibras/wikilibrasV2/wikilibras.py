# -*- coding: utf-8 -*-
from flask import request, make_response
from werkzeug import secure_filename
import json
import pbclient
import os
import pyutil
import requests
import tempfile
import shutil

class Wikilibras:

    def __init__(self, configuration, template_env):
       self.config = configuration
       self.env = template_env
       self.__setup_pb_client()

    def __create_tasks(self, project):
        test_signs = ["ENSINADO", "ENTANTO", "ENTENDIDO"]
        for sign in test_signs:
            video_ref = "/videos/" + sign + "_REF.webm"
            task = dict(sign_name=sign, submission_date=pyutil.get_date_now(), video_ref=video_ref)
            pbclient.create_task(project.id, task)

    def __setup_pb_client(self):
       pbclient.set('endpoint', self.config['PYBOSSA_ENDPOINT'])
       pbclient.set('api_key', self.config['PYBOSSA_API_KEY'])

    def __find_project(self, app_short_name):
        projects = pbclient.find_project(short_name=app_short_name)
        return projects[0] if len(projects) > 0 else None

    def __setup_project(self, project):
        self.__update_project_info(project)

    def __update_project_info(self, project):
        template = self.env.get_template('index.html')
        project.info['task_presenter'] = template.render(
            server = self.config['HOST_STATIC_FILES_ENDPOINT'],
            server_backend = self.config['HOST_ENDPOINT'],
            app_shortname = self.config['PYBOSSA_APP_SHORT_NAME'],
            api_host = self.config['API_HOST'],
            homepage_url = self.config['HOMEPAGE_URL'],
            upload_sign_host = self.config['API_DB_HOST']
        )
        project.info['thumbnail'] = self.config['HOST_STATIC_FILES_ENDPOINT'] + "/img/thumbnail.png"
        project.info['sched'] = "incremental"
        project.info['published'] = True
        project.allow_anonymous_contributors = False
        project.description = self.config['PYBOSSA_APP_DESCRIPTION']
        pbclient.update_project(project)

    def create_project(self):
        app_short_name = self.config['PYBOSSA_APP_SHORT_NAME']
        project = self.__find_project(app_short_name)
        result_msg = ""
        if (project):
            result_msg = "The project " + app_short_name + " was already created."
        else:
            project = pbclient.create_project(self.config['PYBOSSA_APP_NAME'], app_short_name, self.config['PYBOSSA_APP_DESCRIPTION'])
            if (project):
                self.__setup_project(project)
                result_msg = "The project " + app_short_name + " was created."
            else:
                result_msg = "The project " + app_short_name + " couldn't be created. Check the server log for details."
        pyutil.log(result_msg)
        return result_msg

    def update_project(self):
        app_short_name = self.config['PYBOSSA_APP_SHORT_NAME']
        project = self.__find_project(app_short_name)
        # self.__create_tasks(project)
        self.__update_project_info(project)
        result_msg = "The project " + app_short_name + " was updated."
        pyutil.log(result_msg)
        return result_msg

    def __find_task(self, project_id, task_id):
        tasks = pbclient.find_tasks(project_id, id=task_id)
        return tasks[0] if len(tasks) > 0 else None

    def __find_taskruns(self, project_id, task_id):
        return pbclient.find_taskruns(project_id, task_id=task_id)

    def __number_of_taskruns(self, project_id, task_id):
        taskruns = self.__find_taskruns(project_id, task_id)
        return len(taskruns)

    def __close_task(self, project_id, task_id):
        pyutil.log("Closing the task with ID=" + str(task_id) + ".")
        task = self.__find_task(project_id, task_id)
        number_of_taskruns = self.__number_of_taskruns(project_id, task_id)
        task.n_answers = number_of_taskruns + 1
        pbclient.update_task(task)
        return "The task with ID=" + str(task_id) + " was closed."

    def get_file(self, url, filename):
        r = requests.get(url, stream = True)
        if (r.status_code == 200):
            with open(filename, 'wb') as f:
                for chunk in r.iter_content(chunk_size = 1024):
                    if chunk:
                        f.write(chunk)
            return True
        return False

    def finish_task(self):
        task_id = request.form['task_id']
        api_host = self.config['API_HOST']
        api_dbhost = self.config['API_DB_HOST']
        user_id = request.form['user_id']
        sign_name = request.form['sign_name']
        project_id = request.form['project_id']
        number_of_approval = int(request.form['number_of_approval'])
        agreement_number = self.config['AGREEMENT_NUMBER']
        result_msg = ""
        code = 200
        if (number_of_approval >= agreement_number):
            tmp_dir = tempfile.NamedTemporaryFile().name
            if not os.path.exists(tmp_dir):
                os.makedirs(tmp_dir)
            blend_path = os.path.join(tmp_dir, sign_name + ".blend")
            video_path = os.path.join(tmp_dir, sign_name + ".webm")
            blend_url = '%s/public/%s/%s.blend' % (api_host, user_id, sign_name)
            video_url = '%s/public/%s/%s.webm' % (api_host, user_id, sign_name)
            blend_downloaded = self.get_file(blend_url, blend_path)
            video_downloaded = self.get_file(video_url, video_path)
            if (not blend_downloaded):
                pyutil.log("blend file: %s was not downloaded" % (blend_url))
            elif (not video_downloaded):
                pyutil.log("video file: %s was not downloaded" % (video_url))
            else:
                files = [
                    ("video", (video_path, open(video_path, "rb"))),
                    ("video", (blend_path, open(blend_path, "rb")))
                ]
                body = {
                    "nome": sign_name,
                    "idtask": task_id,
                    "selo": 1
                }
                r = requests.post(
                    "%s/updatesinal" % (api_dbhost),
                    files=files,
                    data=body
                )
                shutil.rmtree(tmp_dir)
                code = r.status_code
                if (code == 200):
                    result_msg = self.__close_task(project_id, task_id)
                else:
                    result_msg = r.text
        else:
            result_msg = "The task with ID=" + str(task_id) + " didn't reach the agreement number yet."
        pyutil.log(str(result_msg).encode("UTF-8", errors="ignore"))
        return make_response(result_msg, code)
