--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE public.alembic_version OWNER TO pybossa;

--
-- Name: auditlog; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE auditlog (
    id integer NOT NULL,
    project_id integer NOT NULL,
    project_short_name text NOT NULL,
    user_id integer NOT NULL,
    user_name text NOT NULL,
    created text NOT NULL,
    action text NOT NULL,
    caller text NOT NULL,
    attribute text NOT NULL,
    old_value text,
    new_value text
);


ALTER TABLE public.auditlog OWNER TO pybossa;

--
-- Name: auditlog_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE auditlog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auditlog_id_seq OWNER TO pybossa;

--
-- Name: auditlog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE auditlog_id_seq OWNED BY auditlog.id;


--
-- Name: blogpost; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE blogpost (
    id integer NOT NULL,
    created text,
    project_id integer NOT NULL,
    user_id integer,
    title character varying(255) NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.blogpost OWNER TO pybossa;

--
-- Name: blogpost_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE blogpost_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blogpost_id_seq OWNER TO pybossa;

--
-- Name: blogpost_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE blogpost_id_seq OWNED BY blogpost.id;


--
-- Name: category; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE category (
    id integer NOT NULL,
    name text NOT NULL,
    short_name text NOT NULL,
    description text NOT NULL,
    created text
);


ALTER TABLE public.category OWNER TO pybossa;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO pybossa;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- Name: profile; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE profile (
    id integer NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    access text[] NOT NULL,
    created text
);


ALTER TABLE public.profile OWNER TO pybossa;

--
-- Name: profile_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profile_id_seq OWNER TO pybossa;

--
-- Name: profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE profile_id_seq OWNED BY profile.id;


--
-- Name: project; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE project (
    id integer NOT NULL,
    created text,
    updated text,
    name character varying(255) NOT NULL,
    short_name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    long_description text,
    webhook text,
    allow_anonymous_contributors boolean,
    published boolean NOT NULL,
    featured boolean NOT NULL,
    contacted boolean NOT NULL,
    owner_id integer NOT NULL,
    category_id integer NOT NULL,
    info json
);


ALTER TABLE public.project OWNER TO pybossa;

--
-- Name: project_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_id_seq OWNER TO pybossa;

--
-- Name: project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE project_id_seq OWNED BY project.id;


--
-- Name: result; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE result (
    id integer NOT NULL,
    created text,
    project_id integer NOT NULL,
    task_id integer NOT NULL,
    task_run_ids integer[] NOT NULL,
    last_version boolean,
    info json
);


ALTER TABLE public.result OWNER TO pybossa;

--
-- Name: result_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.result_id_seq OWNER TO pybossa;

--
-- Name: result_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE result_id_seq OWNED BY result.id;


--
-- Name: task; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE task (
    id integer NOT NULL,
    created text,
    project_id integer NOT NULL,
    state text,
    quorum integer,
    calibration integer,
    priority_0 double precision,
    info json,
    n_answers integer
);


ALTER TABLE public.task OWNER TO pybossa;

--
-- Name: task_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_id_seq OWNER TO pybossa;

--
-- Name: task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE task_id_seq OWNED BY task.id;


--
-- Name: task_run; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE task_run (
    id integer NOT NULL,
    created text,
    project_id integer NOT NULL,
    task_id integer NOT NULL,
    user_id integer,
    user_ip text,
    finish_time text,
    timeout integer,
    calibration integer,
    info json
);


ALTER TABLE public.task_run OWNER TO pybossa;

--
-- Name: task_run_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE task_run_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_run_id_seq OWNER TO pybossa;

--
-- Name: task_run_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE task_run_id_seq OWNED BY task_run.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE "user" (
    id integer NOT NULL,
    created text,
    email_addr character varying(254) NOT NULL,
    name character varying(254) NOT NULL,
    fullname character varying(500) NOT NULL,
    locale character varying(254) NOT NULL,
    api_key character varying(36),
    passwd_hash character varying(254),
    admin boolean,
    pro boolean,
    privacy_mode boolean NOT NULL,
    category integer,
    flags integer,
    twitter_user_id bigint,
    facebook_user_id bigint,
    google_user_id character varying,
    ckan_api character varying,
    newsletter_prompted boolean,
    valid_email boolean,
    confirmation_email_sent boolean,
    subscribed boolean,
    info json,
    profile_id integer
);


ALTER TABLE public."user" OWNER TO pybossa;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO pybossa;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: webhook; Type: TABLE; Schema: public; Owner: pybossa; Tablespace: 
--

CREATE TABLE webhook (
    id integer NOT NULL,
    created text,
    updated text,
    project_id integer NOT NULL,
    payload json,
    response text,
    response_status_code integer
);


ALTER TABLE public.webhook OWNER TO pybossa;

--
-- Name: webhook_id_seq; Type: SEQUENCE; Schema: public; Owner: pybossa
--

CREATE SEQUENCE webhook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.webhook_id_seq OWNER TO pybossa;

--
-- Name: webhook_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pybossa
--

ALTER SEQUENCE webhook_id_seq OWNED BY webhook.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY auditlog ALTER COLUMN id SET DEFAULT nextval('auditlog_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY blogpost ALTER COLUMN id SET DEFAULT nextval('blogpost_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY profile ALTER COLUMN id SET DEFAULT nextval('profile_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY project ALTER COLUMN id SET DEFAULT nextval('project_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY result ALTER COLUMN id SET DEFAULT nextval('result_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY task ALTER COLUMN id SET DEFAULT nextval('task_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY task_run ALTER COLUMN id SET DEFAULT nextval('task_run_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY webhook ALTER COLUMN id SET DEFAULT nextval('webhook_id_seq'::regclass);


--
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY alembic_version (version_num) FROM stdin;
2dcee6dfae9d
\.


--
-- Data for Name: auditlog; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY auditlog (id, project_id, project_short_name, user_id, user_name, created, action, caller, attribute, old_value, new_value) FROM stdin;
\.


--
-- Name: auditlog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('auditlog_id_seq', 1, false);


--
-- Data for Name: blogpost; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY blogpost (id, created, project_id, user_id, title, body) FROM stdin;
\.


--
-- Name: blogpost_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('blogpost_id_seq', 1, false);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY category (id, name, short_name, description, created) FROM stdin;
1	Thinking	thinking	Volunteer Thinking projects	2017-07-07T00:22:13.632647
2	Volunteer Sensing	sensing	Volunteer Sensing projects	2017-07-07T00:22:13.633788
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('category_id_seq', 2, true);


--
-- Data for Name: profile; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY profile (id, name, description, access, created) FROM stdin;
1	Colaborador	Colaborador geral	{wikilibras}	2017-07-07T00:22:13.646371
2	Animador	Animador 3D	{wikilibras,corretor_sinais}	2017-07-07T00:22:13.647565
3	Especialista	Especialista em LIBRAS	{wikilibras,validador_sinais}	2017-07-07T00:22:13.648150
\.


--
-- Name: profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('profile_id_seq', 3, true);


--
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY project (id, created, updated, name, short_name, description, long_description, webhook, allow_anonymous_contributors, published, featured, contacted, owner_id, category_id, info) FROM stdin;
\.


--
-- Name: project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('project_id_seq', 1, false);


--
-- Data for Name: result; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY result (id, created, project_id, task_id, task_run_ids, last_version, info) FROM stdin;
\.


--
-- Name: result_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('result_id_seq', 1, false);


--
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY task (id, created, project_id, state, quorum, calibration, priority_0, info, n_answers) FROM stdin;
\.


--
-- Name: task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('task_id_seq', 1, false);


--
-- Data for Name: task_run; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY task_run (id, created, project_id, task_id, user_id, user_ip, finish_time, timeout, calibration, info) FROM stdin;
\.


--
-- Name: task_run_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('task_run_id_seq', 1, false);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY "user" (id, created, email_addr, name, fullname, locale, api_key, passwd_hash, admin, pro, privacy_mode, category, flags, twitter_user_id, facebook_user_id, google_user_id, ckan_api, newsletter_prompted, valid_email, confirmation_email_sent, subscribed, info, profile_id) FROM stdin;
1	2017-01-01T00:00:00.000000	wikilibras@lavid.ufpb.br	wikilibras	wikilibras	pt_BR	2324bc23-7d6f-4840-8905-b1e6c1675eed	pbkdf2:sha1:1000$wIP6vkOx$99be5c325961aa39030bb10e3b58a85ac3bfaa90	t	f	f	\N	\N	\N	\N	\N	\N	f	t	f	t	{}	1
2	2017-01-01T00:00:00.000000	tester1@lavid.ufpb.br	tester1	tester1	pt_BR	e4fef6e0-d954-4ce8-be39-ab08a96aa61a	pbkdf2:sha256:50000$ZXnRVTb5$2bfd28d77b26d3c1f183192ec793885bbd523ad92b8a6708edce6d5226a850fe	t	f	f	\N	\N	\N	\N	\N	\N	f	t	f	t	{}	1
3	2017-01-01T00:00:00.000000	tester2@lavid.ufpb.br	tester2	tester2	pt_BR	1d0fbea5-88d7-40f2-a835-676bc3824f8c	pbkdf2:sha256:50000$DizF7nb3$d4b4946fcac8a88cb2337d610e743432441719559f236b0d5816feb9ffbbefdf	t	f	f	\N	\N	\N	\N	\N	\N	f	t	f	t	{}	1
4	2017-01-01T00:00:00.000000	tester3@lavid.ufpb.br	tester3	tester3	pt_BR	8c5707aa-e813-4a5e-b3f1-7d3e413da494	pbkdf2:sha256:50000$pYfjlhqL$a3bbd613e39c0ef6387548b4ef1c8b273bf02031b34b10d2f26f21f0839da890	t	f	f	\N	\N	\N	\N	\N	\N	f	t	f	t	{}	1
5	2017-01-01T00:00:00.000000	tester4@lavid.ufpb.br	tester4	tester4	pt_BR	f47796e1-e0da-49ff-b030-ad7459cf464b	pbkdf2:sha256:50000$Zht8o4X9$b4e630fb43254589c407406836bf60b5e669f7368dfa45a24ebbc0ed5fee3872	t	f	f	\N	\N	\N	\N	\N	\N	f	t	f	t	{}	1
6	2017-01-01T00:00:00.000000	tester5@lavid.ufpb.br	tester5	tester5	pt_BR	7dce7aac-29cd-467f-a1b6-cc96044b5909	pbkdf2:sha256:50000$v6cDxeNk$2f7b494d9d4b2e175cdfc9e617758a3a6c30e511385bc24e4d1b499064f9a908	t	f	f	\N	\N	\N	\N	\N	\N	f	t	f	t	{}	1
\.


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('user_id_seq', 6, true);


--
-- Data for Name: webhook; Type: TABLE DATA; Schema: public; Owner: pybossa
--

COPY webhook (id, created, updated, project_id, payload, response, response_status_code) FROM stdin;
\.


--
-- Name: webhook_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pybossa
--

SELECT pg_catalog.setval('webhook_id_seq', 1, false);


--
-- Name: alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: auditlog_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY auditlog
    ADD CONSTRAINT auditlog_pkey PRIMARY KEY (id);


--
-- Name: blogpost_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY blogpost
    ADD CONSTRAINT blogpost_pkey PRIMARY KEY (id);


--
-- Name: category_name_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_name_key UNIQUE (name);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: category_short_name_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_short_name_key UNIQUE (short_name);


--
-- Name: profile_name_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY profile
    ADD CONSTRAINT profile_name_key UNIQUE (name);


--
-- Name: profile_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (id);


--
-- Name: project_name_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY project
    ADD CONSTRAINT project_name_key UNIQUE (name);


--
-- Name: project_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- Name: project_short_name_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY project
    ADD CONSTRAINT project_short_name_key UNIQUE (short_name);


--
-- Name: result_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY result
    ADD CONSTRAINT result_pkey PRIMARY KEY (id);


--
-- Name: task_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: task_run_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY task_run
    ADD CONSTRAINT task_run_pkey PRIMARY KEY (id);


--
-- Name: user_api_key_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_api_key_key UNIQUE (api_key);


--
-- Name: user_ckan_api_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_ckan_api_key UNIQUE (ckan_api);


--
-- Name: user_email_addr_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_addr_key UNIQUE (email_addr);


--
-- Name: user_facebook_user_id_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_facebook_user_id_key UNIQUE (facebook_user_id);


--
-- Name: user_google_user_id_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_google_user_id_key UNIQUE (google_user_id);


--
-- Name: user_name_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_name_key UNIQUE (name);


--
-- Name: user_passwd_hash_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_passwd_hash_key UNIQUE (passwd_hash);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_twitter_user_id_key; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_twitter_user_id_key UNIQUE (twitter_user_id);


--
-- Name: webhook_pkey; Type: CONSTRAINT; Schema: public; Owner: pybossa; Tablespace: 
--

ALTER TABLE ONLY webhook
    ADD CONSTRAINT webhook_pkey PRIMARY KEY (id);


--
-- Name: blogpost_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY blogpost
    ADD CONSTRAINT blogpost_project_id_fkey FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE;


--
-- Name: blogpost_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY blogpost
    ADD CONSTRAINT blogpost_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: project_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY project
    ADD CONSTRAINT project_category_id_fkey FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: project_owner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY project
    ADD CONSTRAINT project_owner_id_fkey FOREIGN KEY (owner_id) REFERENCES "user"(id);


--
-- Name: result_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY result
    ADD CONSTRAINT result_project_id_fkey FOREIGN KEY (project_id) REFERENCES project(id);


--
-- Name: result_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY result
    ADD CONSTRAINT result_task_id_fkey FOREIGN KEY (task_id) REFERENCES task(id) ON DELETE CASCADE;


--
-- Name: task_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY task
    ADD CONSTRAINT task_project_id_fkey FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE;


--
-- Name: task_run_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY task_run
    ADD CONSTRAINT task_run_project_id_fkey FOREIGN KEY (project_id) REFERENCES project(id);


--
-- Name: task_run_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY task_run
    ADD CONSTRAINT task_run_task_id_fkey FOREIGN KEY (task_id) REFERENCES task(id) ON DELETE CASCADE;


--
-- Name: task_run_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY task_run
    ADD CONSTRAINT task_run_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: user_profile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_profile_id_fkey FOREIGN KEY (profile_id) REFERENCES profile(id);


--
-- Name: webhook_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pybossa
--

ALTER TABLE ONLY webhook
    ADD CONSTRAINT webhook_project_id_fkey FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

