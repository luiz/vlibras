(function () {
    'use strict';

    angular.module('wikilibras.progress').controller('progressCtrl', ['$scope', 'progressService', function ($scope, progressService) {
        $scope.progress = {"wikilibras": initProgressData(), "corretor_sinais": initProgressData(), "validador_sinais": initProgressData(), 'uploaded_signs': initProgressData()};

        function initProgressData() {
            return {"n_tasks": 0, "n_task_runs": 0, "n_completed_tasks": 0, "last_activity": new Date(), "goal_n_tasks": 0, "goal_deadline": new Date()};
        }

        progressService.getProjectsProgressData().then(function(response) {
            angular.forEach(response, function(progress) {
                var projectProgress = $scope.progress[progress.short_name];
                if (!projectProgress || typeof  projectProgress === "undefined") return;
                projectProgress.n_tasks = progress.n_tasks;
                projectProgress.n_task_runs = progress.n_task_runs;
                projectProgress.n_completed_tasks = progress.n_completed_tasks;
                projectProgress.last_activity = progress.last_activity;
                projectProgress.goal_n_tasks = progress.goal_n_tasks;
                projectProgress.goal_deadline = progress.goal_deadline;
            });
            console.log($scope.progress);
        });
    }]);
}());