(function () {
    'use strict';

    angular.module('wikilibras.ranking').controller('rankingCtrl', ['$scope', 'rankingService', function ($scope, rankingService) {
        $scope.activeTab = 0;
        $scope.rankingData = {};

        $scope.setActiveTab = function(tab) {
            $scope.activeTab = tab;
        };

        rankingService.getRankingData().then(function(response) {
            $scope.rankingData = response;
        });
    }]);
}());