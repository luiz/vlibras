(function () {
    'use strict';

    angular.module('wikilibras.signs').controller('signsCtrl', ['$scope', 'signsService', function ($scope, signsService) {
        $scope.signsData = [];
        $scope.activeSignId = -1;

        signsService.getSignsData().then(function(response) {
            $scope.signsData = response;
        });
        
        $scope.getSignData = function(id) {
            return $scope.signsData[id];
        };

        $scope.showSignModal = function(id) {
            $scope.activeSignId = id;
            $('.sign-modal').modal('show');
        };
    }]);
}());