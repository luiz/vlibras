(function () {
    'use strict';

    angular.module('wikilibras.signs').service("signsService", ['$http', '$sce', function ($http, $sce) {
        var PROJECT_CONF_URL = 'conf/app-conf.json';
        var PROJECT_LAST_ANSWERS_ENDPOINT =  '/api/project_last_answers';

        function buildSignUrl(baseUrl, parameterJson) {
            var signUrl = baseUrl + '/public/' + parameterJson.userId + '/' + parameterJson.sinal;
            return [{'path': $sce.trustAsResourceUrl(signUrl + '.webm'), 'type': 'video/webm'}, {'path': $sce.trustAsResourceUrl(signUrl + '.mp4'), 'type': 'video/mp4'}];
        }

        return {
            getSignsData: function() {
                return $http.get(PROJECT_CONF_URL).then(function(response) {
                    var basePyBossaApiUrl = response.data.pybossa_url;
                    var baseWikiLibrasApiUrl = response.data.wikilibras_api_url;

                    return $http.get(basePyBossaApiUrl + PROJECT_LAST_ANSWERS_ENDPOINT).then(function(response) {
                        var projects = response.data;
                        var signsData = [];
                        angular.forEach(projects, function(data) {
                            if (data.project_name !== 'wikilibras') return;
                            angular.forEach(data.last_answers, function(answer) {
                                var signData = {};
                                signData.signName = answer.taskrun_info.parameter_json.sinal;
                                signData.userId = answer.taskrun_info.parameter_json.userId;
                                signData.sources = buildSignUrl(baseWikiLibrasApiUrl, answer.taskrun_info.parameter_json);
                                signsData.push(signData);
                            });
                        });
                        console.log(signsData);
                        return signsData;
                    });
                });
            }
        };
    }]);
}());