(function () {
    'use strict';

    angular.module('wikilibras', ['wikilibras.progress', 'wikilibras.ranking', 'wikilibras.volunteers', 'wikilibras.signs', 'wikilibras.presentation', 'duScroll', 'angularMoment', 'ngSanitize']).value('duScrollOffset', 70);
}());