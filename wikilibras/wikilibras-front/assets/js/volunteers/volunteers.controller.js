(function () {
    'use strict';

    angular.module('wikilibras.volunteers').controller('volunteersCtrl', ['$scope', 'volunteersService', function ($scope, volunteersService) {
        $scope.volunteersData = {};
        volunteersService.getVolunteersData().then(function(response) {
            $scope.volunteersData = response;
        });
    }]);
}());