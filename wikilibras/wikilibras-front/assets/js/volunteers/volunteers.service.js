(function () {
    'use strict';

    angular.module('wikilibras.volunteers').service("volunteersService", ['$http', function ($http) {
        var PROJECT_CONF_URL = 'conf/app-conf.json';
        var VOLUNTEERS_ENDPOINT =  '/api/user';

        return {
            getVolunteersData: function() {
                return $http.get(PROJECT_CONF_URL).then(function(response) {
                    var baseApiUrl = response.data.pybossa_url;
                    return $http.get(baseApiUrl + VOLUNTEERS_ENDPOINT).then(function(response) {
                        var volunteersData = {};
                        volunteersData['users_data'] = response.data;
                        volunteersData['avatars_data'] = [];

                        angular.forEach(volunteersData['users_data'], function(data) {
                            if (typeof data.info === 'undefined') return;
                            data.info['avatar_url'] = data.info.container && data.info.avatar? baseApiUrl + '/uploads/' + data.info.container + '/' + data.info.avatar : '';
                            if (data.info['avatar_url'] !== '') {
                                volunteersData['avatars_data'].push(data.info['avatar_url']);
                            }
                        });
                        console.log(volunteersData);
                        return volunteersData;
                    });
                });
            }
        };
    }]);
}());