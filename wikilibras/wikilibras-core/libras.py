# -*- coding: UTF-8 -*-

# importa modulo do Blender
import bpy

# importa modulos do Python
import io
import json
import sys
import os

# define o caminho absoluto do diretorio deste arquivo
getcwd = os.path.dirname(os.path.abspath(__file__))

# insere o caminho do diretorio atual no path (permite o acesso aos modulos locais)
sys.path.insert(0, getcwd)

# importa modulos locais
import util
import moves
import pyutil
import decode

def main():
    util.set_pose_mode()
    util.delete_all_keyframes()
    util.configure_output()
    bpy.context.scene.animation_data_create()

    try:
        js_input = json.loads(sys.argv[7])
        js_movimentos = js_input["movimentos"]
        frame_jump = util.dict_interpolation[js_input["interpolacao"]]

        endFrame = util.pose_default(0, frame_jump)
        timeline_facial = 0
        timeline_mao_esquerda = 0
        timeline_mao_direita = 0

        for i in range(0, len(js_movimentos)):
            timeline_facial = endFrame
            timeline_mao_esquerda = endFrame
            timeline_mao_direita = endFrame

            # tenta decodificar objetos JSON
            try:
                js_facial = js_movimentos[i]["facial"]
            except:
                js_facial = {}
            try:
                js_mao_esquerda = js_movimentos[i]["mao_esquerda"]
            except:
                js_mao_esquerda = {}
            try:
                js_mao_direita = js_movimentos[i]["mao_direita"]
            except:
                js_mao_direita = {}

            # faz tratamento dos objetos
            if (js_facial == {}):
                pyutil.log("movimento [%d] Exp facial = <Vazio>" % (i))
            else:
                pyutil.log("movimento [%d] Exp facial = [%d]" % (i, js_facial["expressao"]))
                timeline_facial = decode.facial(js_facial, timeline_facial, frame_jump)

            if (js_mao_esquerda == {}):
                pyutil.log("movimento [%d] Mao Esquerda = <Vazio>" % (i))
                # TODO posicionar mao esquerda na lateral do corpo
            else:
                pyutil.log("movimento [%d] Mao Esquerda = [%s]" % (i, next(iter(js_mao_esquerda.keys()))))
                timeline_mao_esquerda = decode.hand_mov(timeline_mao_esquerda, frame_jump, js_mao_esquerda, False)

            if (js_mao_direita == {}):
                pyutil.log("movimento [%d] Mao Direita = <Vazio>" % (i))
                # TODO posicionar mao direita na lateral do corpo
            else:
                pyutil.log("movimento [%d] Mao Direita = [%s]" % (i, next(iter(js_mao_direita.keys()))))
                timeline_mao_direita = decode.hand_mov(timeline_mao_direita, frame_jump, js_mao_direita, True)

            endFrame = max(timeline_facial, timeline_mao_esquerda, timeline_mao_direita)
            endFrame += frame_jump

        # setar pose padrao final em todos os bones (location e rotation)
        endFrame = util.pose_default(endFrame, frame_jump)

        sinal = str(js_input["sinal"])
        user_id = str(js_input["userId"])
        directory = os.path.join(getcwd, "users", user_id)

        # exporta .blend
        util.export_blend(directory, sinal + ".blend", 0, 0, endFrame)

        # exporta .json
        # util.export_json(js_input, directory, sinal + ".json")

        # exporta .mp4
        util.render_sign(user_id, sinal, endFrame)

    except:
        pyutil.print_stack_trace()
        raise

if __name__ == "__main__":
    main()
