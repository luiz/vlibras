# -*- coding: UTF-8 -*-

import subprocess
import sys
import os

getcwd = os.path.dirname(os.path.abspath(__file__))

import pyutil

blend_path = os.path.join(getcwd, "avatar_cartoon_v2.74.blend")
main_path = os.path.join(getcwd, "libras.py")

result = 0

try:
    # para gravar logs do blender, usar codigo abaixo
    # result = subprocess.call(['blender', '-b', blend_path, '-P', main_path, '-noaudio', '--', sys.argv[1]], stdout = open('bpy.log', 'w'))
    result = subprocess.call(['blender', '-b', blend_path, '-P', main_path, '-noaudio', '--', sys.argv[1]])
except:
    result = pyutil.print_stack_trace()

exit(result)
