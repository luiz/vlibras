(function(videoHelper, $, undefined) {

	function _getSource(videoBaseUrl) {
		return '<source src="' + encodeURI(videoBaseUrl + '.webm') + '" type="video/webm">'
		+ '<source src="' + encodeURI(videoBaseUrl + '.mp4') + '" type="video/mp4">Sem suporte a vídeos';
	}

	function _getSourceByWebmUrl(webmUrl) {
		var mp4Url = webmUrl.replace('.webm', '.mp4');
		return '<source src="' + encodeURI(webmUrl) + '" type="video/webm">'
				+ '<source src="' + encodeURI(mp4Url) + '" type="video/mp4">Sem suporte a vídeos';
	}

	function _controlVideo(elId, toPlay) {
		if ($(elId).length === 0)
			return;
		if (toPlay) {
			$(elId).get(0).play();
		} else {
			$(elId).get(0).pause();
		}
	}

	videoHelper.play = function(elId) {
		_controlVideo(elId, true);
	};

	videoHelper.pause = function(elId) {
		_controlVideo(elId, false);
	};

	videoHelper.getSource = function(videoBaseUrl) {
		return _getSource(videoBaseUrl);
	};

	videoHelper.getSourceByWebmUrl = function(webmUrl) {
		return _getSourceByWebmUrl(webmUrl);
	};

}(window.videoHelper = window.videoHelper || {}, jQuery));
