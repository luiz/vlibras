(function(corretor, $, undefined) {

	var baseUrl = '';
	var serverBackendUrl = '';
	var apiDBHostUrl = '';
	var uploadsUrl = '';
	var uploadSessionId = _getUploadSessionID();
	var pybossaEndpoint = '';
	var projectName = '';
	var currentTaskId = -1;

	function _getUploadSessionID() {
		var corretorSessionId = Cookies.get('corretor_session_id');
		var uploadSessionId = '';
		if (typeof corretorSessionId == 'undefined') {
			uploadSessionId = _createUploadSessionID();
		} else {
			var pybossaRembemberToken = Cookies.get('remember_token');
			var splittedSessionId = corretorSessionId.split(';');
			var lastRememberToken = splittedSessionId[0];
			var uploadSessionId = splittedSessionId[1];
			if (pybossaRembemberToken != lastRememberToken) {
				uploadSessionId = _createUploadSessionID();
			}
		}
		return uploadSessionId;
	}

	function _getLoggedUser() {
		var pybossaRememberToken = Cookies.get('remember_token');
		var splittedTokenId = pybossaRememberToken.split('|');
		return splittedTokenId.length > 0 ? splittedTokenId[0] : 'anonymous';
	}

	function _createUploadSessionID() {
		var uploadSessionId = _generateSessionID();
		var pybossaRembemberToken = Cookies.get('remember_token');
		corretorSessionId = pybossaRembemberToken + ';' + uploadSessionId;
		Cookies.set('corretor_session_id', corretorSessionId, {
			path : '/',
			expires : 365
		});
		return uploadSessionId;
	}

	function _generateSessionID() {
		return (Math.random() + ' ').substring(2, 10)
				+ (Math.random() + ' ').substring(2, 10);
	}

	function _resetUploadProgress() {
		$('#upload-progress .progress-bar').css('width', '0%');
	}

	function _enableFinishButton(task, deferred) {
		$('#finish-button').removeClass('disabled-button');
		$('#finish-button').addClass('enabled-button');
		$('#finish-button').off('click').on('click', function() {
			_submitAnswer(task, deferred, 'FIXED');
		});
	}

	function _enableLoading() {
		$('#loading-container').show();
		$('#main-container').addClass('mask');
	}

	function _disableLoading() {
		$('#loading-container').hide();
		$('#main-container').removeClass('mask');
	}

	function _disableFinishButton() {
		$('#finish-button').off('click');
		$('#finish-button').removeClass('enabled-button');
		$('#finish-button').addClass('disabled-button');
	}

	function _disableApprovalButton() {
		$('#approval-button').off('click');
		$('#approval-button').removeClass('enabled-button');
		$('#approval-button').addClass('disabled-button');
	}

	function _showApprovalGUI(task, deferred) {
		$('#upload-container').hide();
		$('.link-container').hide();
		$('#finish-button').hide();
		$('#approval-button').show();
		$('#fix-button').show();
		_enableFinishButton(task, deferred);
	}

	function _showFixGUI() {
		_disableFinishButton();
		$('#upload-container').show();
		$('.link-container').show();
		$('#finish-button').show();
		$('#approval-button').hide();
		$('#fix-button').hide();
	}

	function _setupGUI(task, deferred) {
		_disableFinishButton();
		_resetUploadProgress();
		$('#upload-file-name').text('Escolha um arquivo');
		$('#file-upload').val('');
		$('#file-upload').fileupload(
				{
					url : serverBackendUrl + '/upload',
					formData : {
						'upload_session_id' : uploadSessionId,
						'sign_name' : task.info.sign_name
					},
					add : function(e, data) {
						_resetUploadProgress();
						$('#upload-file-name').text(data.files[0].name)
						data.submit();
					},
					done : function(e, data) {
						_enableFinishButton(task, deferred);
					},
					progressall : function(e, data) {
						var progress = parseInt(data.loaded / data.total * 100,
								10);
						$('#upload-progress .progress-bar').css('width',
								progress + '%');
					},
					error : function(error) {
						alert(error.responseText);
						_resetUploadProgress();
					}
				});

		$('#upload-button').off('click').on('click', function() {
			$('#file-upload').click();
		});
		$('#skip-button').off('click').on('click', function() {
			_submitAnswer(task, deferred, 'SKIPPED');
		});
		$('#approval-button').off('click').on('click', function() {
			_submitAnswer(task, deferred, 'APPROVED');
		});
		$('#fix-button').off('click').on('click', function() {
			_showFixGUI();
		});
	}

	function _createAnswer(task, status) {
		var answer = {};
		var lastAnswer = task.info.last_answer;
		var hasLastAnswer = typeof lastAnswer != 'undefined';
		if (hasLastAnswer) {
			answer = lastAnswer;
		} else {
			answer = {
				'number_of_fixes' : 0,
				'number_of_approval' : 0
			};
		}
		answer['status'] = status;
		if (status == 'FIXED') {
			answer['last_edit_date'] = moment(new Date()).format(
					'YYYY-MM-DDTHH:mm:ss')
			answer['upload_session_id'] = uploadSessionId;
			answer['number_of_fixes'] = answer.number_of_fixes + 1;
			answer['number_of_approval'] = 0;
		} else if (status == 'APPROVED') {
			answer['number_of_approval'] = answer.number_of_approval + 1;
		}
		return answer;
	}

	function _renderVideo(task, deferred, answer) {
		_enableLoading();
		$.ajax({
			type : 'POST',
			url : serverBackendUrl + '/render_video',
			data : {
				'upload_session_id' : uploadSessionId,
				'sign_name' : task.info.sign_name
			},
			success : function(response) {
				_disableLoading();
				_saveAnswer(task, deferred, answer);
			},
			error : function(xhr, textStatus, error) {
				_disableLoading();
				_disableFinishButton();
				alert(xhr.responseText);
			}
		});
	}

	function _finishTask(task, deferred, answer) {
		_enableLoading();
		var lastAnswer = task.info.last_answer;
		var hasLastAnswer = typeof lastAnswer != 'undefined';
		var toSubmitUploadSessionId = hasLastAnswer ? lastAnswer.upload_session_id
				: uploadSessionId;
		$.ajax({
			type : 'POST',
			url : serverBackendUrl + '/finish_task',
			data : {
				'task_id' : task.id,
				'project_id' : task.project_id,
				'sign_name' : task.info.sign_name,
				'upload_session_id' : toSubmitUploadSessionId,
				'number_of_approval' : answer.number_of_approval
			},
			success : function(response) {
				_disableLoading();
				_saveAnswer(task, deferred, answer);
			},
			error : function(xhr, textStatus, error) {
				_disableLoading();
				_disableApprovalButton();
				alert(xhr.responseText);
			}
		});
	}

	function _saveAnswer(task, deferred, answer) {
		pybossa.saveTask(task.id, answer).done(function() {
			$('#success').fadeIn(500);
			$('#corretor-container').hide();
			setTimeout(function() {
				deferred.resolve();
			}, 2000);
		});
	}

	function _submitAnswer(task, deferred, status) {
		var answer = _createAnswer(task, status);
		if (status == 'FIXED') {
			_renderVideo(task, deferred, answer);
		} else if (status == 'APPROVED') {
			_finishTask(task, deferred, answer);
		} else if (status == 'SKIPPED') {
			_saveAnswer(task, deferred, answer);
		}
	}

	function _loadTaskInfo(task, deferred) {
		currentTaskId = task.id;
		var signName = task.info.sign_name;
		var relBlend = task.info.blend;
		var relVideoAvatarUrl = task.info.video_ava;
		var relVideoRefUrl = task.info.video_ref;
		var blendLink = apiDBHostUrl + relBlend;
		var avatarVidUrl = apiDBHostUrl + relVideoAvatarUrl;
		var refVidUrl = apiDBHostUrl + relVideoRefUrl;
		
		$('.sign-label').text(signName);
		$('#submission-date').text(
				moment(task.info.submission_date).format('DD/MM/YYYY'));
		$('#ref-video').html(videoHelper.getSourceByWebmUrl(refVidUrl));
		$('#ref-video-link').attr('href', encodeURI(refVidUrl));

		var lastAnswer = task.info.last_answer;
		var hasLastAnswer = typeof lastAnswer != 'undefined';
		if (hasLastAnswer
				&& typeof lastAnswer.upload_session_id != 'undefined') {
			blendLink = uploadsUrl + lastAnswer.upload_session_id + '/'
					+ signName + '.blend';
			avatarVidUrl = uploadsUrl + lastAnswer.upload_session_id + '/'
			+ signName + '.webm';

			$('#last-edit-date').text(
					moment(lastAnswer.last_edit_date).format('DD/MM/YYYY'));
			$('#number-of-fixes').text(lastAnswer.number_of_fixes);
			_showApprovalGUI(task, deferred);
		}

		$('#blend-link').attr('href', encodeURI(blendLink));
		$('#avatar-video').html(videoHelper.getSourceByWebmUrl(avatarVidUrl));
	}

	function _loadMainComponents() {
		pybossaApiHelper.setup(pybossaEndpoint, projectName);
		loadHtmlHelper.setup(baseUrl);
		ranking.setup(baseUrl, pybossaEndpoint, projectName, _getLoggedUser());
	}

	pybossa.presentTask(function(task, deferred) {
		_loadMainComponents();
		if (!$.isEmptyObject(task) && currentTaskId != task.id) {
			_loadTaskInfo(task, deferred);
			_setupGUI(task, deferred);
			$('#success').hide();
			$('#corretor-container').fadeIn(500);
		} else {
			$('#corretor-container').hide();
			$('#finish').fadeIn(500);
			ranking.tasksEnded();
		}
	});

	// Private methods
	function _run(projectname) {
		pybossa.setEndpoint(pybossaEndpoint);
		pybossa.run(projectname);
	}

	// Public methods
	corretor.run = function(serverhost, serverbackend, projname, apidbhost) {
		baseUrl = serverhost;
		serverBackendUrl = serverbackend;
		apiDBHostUrl = apidbhost;
		uploadsUrl = baseUrl + '/uploads/';
		pybossaEndpoint = '/pybossa';
		projectName = projname;
		_run(projectName);
	};

}(window.corretor = window.corretor || {}, jQuery));