# -*- coding: UTF-8 -*-

import json
import os
import subprocess
import pyutil
import shutil

getcwd=os.path.dirname(os.path.abspath(__file__))
bpy_script_action=os.path.join(getcwd, "bpy_checkout.py")
bpy_script_render=os.path.join(getcwd, "bpy_render.py")

def file_exists(file_path):
    if ((os.path.isfile(file_path) == 1) and (os.path.exists(file_path) == 1)):
        return True
    else:
        return False

def check_action(blend_file="", action_name="", action_fake_is_valid=True, min_frame_count=10, hide_output=True):
    if (file_exists(blend_file)):
        if not ((isinstance(blend_file, str) and isinstance(action_name, str)) or
                (isinstance(blend_file, unicode) and isinstance(action_name, unicode)) or
                ((isinstance(action_fake_is_valid, bool) and isinstance(min_frame_count, int)))):
            pyutil.log("Args to check_action no match types", 4)
            return 1
        if (action_name == ""):
            action_name=os.path.splitext(blend_file)[0]
        try:
            json_object=json.JSONEncoder().encode(
                {
                    "action_name": action_name,
                    "action_fake_is_valid": action_fake_is_valid,
                    "min_frame_count": min_frame_count
                }
            )
            if (hide_output):
                dev_null=open(os.devnull, 'w')
                result_code=subprocess.call(['blender', '-b', blend_file, '-noaudio', '-P', bpy_script_action, "--", json_object], stdout=dev_null, stderr=dev_null)
            else:
                result_code=subprocess.call(['blender', '-b', blend_file, '-noaudio', '-P', bpy_script_action, "--", json_object])
            if (result_code == 4):
                pyutil.log("JSON Key no match", 4)
            elif (result_code == 5):
                pyutil.log("Action in blend file no exists", 4)
            elif (result_code == 6):
                pyutil.log("Action is fake user", 4)
            elif (result_code == 7):
                pyutil.log("Timeline Frame Count is less than 10", 4)
            elif (result_code == 8):
                pyutil.log("Args count no match", 4)
        except:
            pyutil.log("Subprocess interrupt", 4)
            return 2
        return result_code
    else:
        pyutil.log("Blend file not exists", 4)
        return 3

def render_video(upload_dir="", blend_file="", sign_name="", convert_to_webm=True, rm_original_video=True, hide_output=False, convert_to_mp4_web_format=True):
    if (file_exists(blend_file)):
        if not ((isinstance(upload_dir, str) and isinstance(blend_file, str)) or
                (isinstance(upload_dir, unicode) and isinstance(blend_file, unicode)) or
                (isinstance(convert_to_webm, bool) and isinstance(rm_original_video, bool) and isinstance(hide_output, bool))):
            pyutil.log("Args to render_video no match types", 4)
            return 1
        try:
            json_object = json.JSONEncoder().encode(
                {
                    "upload_dir": upload_dir,
                    "sign_name": sign_name
                }
            )
            if (hide_output):
                dev_null = open(os.devnull, 'w')
                result_code = subprocess.call(['blender', '-b', blend_file, '-noaudio', '-P', bpy_script_render, "--", json_object], stdout=dev_null, stderr=dev_null)
            else:
                result_code=subprocess.call(['blender', '-b', blend_file, '-noaudio', '-P', bpy_script_render, "--", json_object])
            if (convert_to_webm):
                video_mp4 = os.path.join(upload_dir, sign_name + ".mp4")
                video_webm = os.path.join(upload_dir, sign_name + ".webm")
                subprocess.call(["avconv", "-loglevel", "0", "-y", "-i", video_mp4, "-r", "24", "-vcodec", "libvpx", video_webm])
            if (convert_to_mp4_web_format):
                video_mp4 = os.path.join(upload_dir, sign_name + ".mp4")
                video_mp4_tmp = os.path.join(upload_dir, sign_name + "-tmp.mp4")
                shutil.copy(video_mp4, video_mp4_tmp)
                subprocess.call(["avconv", "-loglevel", "0", "-y", "-i", video_mp4_tmp, "-r", "24", "-vcodec", "libx264", video_mp4])
                video_mp4 = video_mp4_tmp
            if (rm_original_video):
                subprocess.call(["rm", video_mp4])
            if (result_code == 4):
                pyutil.log("Except in process", 4)
            elif(result_code == 5):
                pyutil.log("Args count no match", 4)
        except:
            pyutil.log("Subprocess interrupt", 4)
            return 2
        return result_code
    else:
        pyutil.log("JSON Key no match", 4)
        return 3

"""
def main():
    print("check_action return:", check_action("ENTANTO_AVATAR.blend", "ENTANTO"))
    # return codes
    # 0: [OK] Blend file and Action Name exists
    # 1: [ERROR] Args to check_action no match types
    # 2: [ERROR] Subprocess interrupt
    # 3: [ERROR] Blend file not exists
    # 4: [ERROR] JSON Key no match
    # 5: [ERROR] Action in blend file no exists
    # 6: [ERROR] Action is fake user
    # 7: [ERROR] Timeline Frame Count is less than 10
    # 8: [ERROR] Args count no match
    print("render_video return:", render_video("ENTANTO_AVATAR.blend", "ENTANTO"))
    # return codes
    # 0: [OK] Video.mp4 generated
    # 1: [ERROR] Args to check_action no match types
    # 2: [ERROR] Subprocess interrupt
    # 3: [ERROR] JSON Key no match
    # 4: [ERROR] Except in process
    # 5: [ERROR] Args count no match
    return

if __name__ == "__main__":
    main()
"""