# VLibras Translate Container

## Requirements

Recommended OS: [Linux Ubuntu 14.04.4 LTS 64-bit](http://releases.ubuntu.com/14.04/).

Dependencies: [Docker](https://www.docker.com/), [RabbitMQ](https://www.rabbitmq.com/),
              [Graylog](https://www.graylog.org/), [Nodejs](https://nodejs.org/en/),
              [MongoDB](https://www.mongodb.com/).

>Note: See Installation section to install dependencies.

You need install some packages: `build-essential`, `git`, `python-pip`,
  `python-setuptools`.

```sh
    $ sudo apt-get update
    $ sudo apt-get install -y build-essential git python-pip python-setuptools
```

## Sources

To get source code, use these command:

```sh
    $ git clone 'git@gitlab.lavid.ufpb.br:vlibras/vlibras-translate-container.git'
```

## Installation and Execution

To install and run vlibras-translate-container for production, use these commands:

```sh
    $ sudo docker build . -t <container_name>
    $ sudo docker run -p 80:80 <image_id>
```

For the development, you need to install dependencies and run the workers manually:

```sh
    $ cd scripts
    $ ./configure
```

```sh
    $ cd ../core
    $ ./translator.py
    $ ./renderer.py
```

## Contributors

* Erickson Silva <erickson.silva@lavid.ufpb.br>
* Jonathan Lincoln Brilhante <jonathan.lincoln.brilhante@gmail.com>
* Wesnydy Lima Ribeiro <wesnydy@lavid.ufpb.br>
