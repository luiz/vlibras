#!/usr/bin/env bash

OK="\033[32mOK\033[0m"
ERROR="echo -e '\033[31mERROR\033[0m'"

set -e

echo -n "Updating apt"
sudo apt-get -qq -y update && echo -n "." || eval $ERROR
sudo apt-get -qq -y upgrade && echo -n "." || eval $ERROR
sudo apt-get -qq -y update && echo -e ". $OK" || eval $ERRO

echo -e "Obtaining RabbitMQ from rabbitmq.com APT Repository..."
echo 'deb http://www.rabbitmq.com/debian/ testing main' | sudo tee /etc/apt/sources.list.d/rabbitmq.list
wget -q -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -
echo -n "Installing RabbitMQ"
sudo apt-get -qq -y update && echo -n "." || eval $ERROR
sudo apt-get install -qq -y rabbitmq-server && echo -n "." || eval $ERROR
sudo pip install -q pika && echo -e ". $OK" || eval $ERROR
echo -e "Enabling RabbitMQ management... "
sudo rabbitmq-plugins enable rabbitmq_management

echo -n "Installing Graylog"
sudo pip install -q graypy && echo -n ".." || eval $ERROR
sudo pip install -q graypy[amqp] && echo -e ". $OK" || eval $ERROR

echo -n "Installing nodejs v4.x"
curl -sL https://deb.nodesource.com/setup_4.x | sudo bash - &>/dev/null
sudo apt-get install -qq -y nodejs && echo -e "... $OK" || eval $ERROR

read -r -p "You want to install VLibras Translate? [Y/n] " response
response=${response,,} #tolower
if [[ $response =~ ^(yes|y| ) ]]; then
  echo -n "Obtaining VLibras Translate"
  cd $HOME
  git clone git@git.lavid.ufpb.br:vlibras-translate.git && echo -e "... $OK" || eval $ERROR
  echo -e "Installing VLibras Translate"
  cd vlibras-translate/scripts/linux
  ./install.sh
fi

read -r -p "You want to install VLibras Video Player? [Y/n] " response
response=${response,,} #tolower
if [[ $response =~ ^(yes|y| ) ]]; then
  echo -n "Obtaining VLibras Video Player"
  wget -q http://150.165.204.30:8080/core/linux/unityVideo.tar.gz && echo -e "... $OK" || eval $ERROR
  echo -n "Installing VLibras Video Player"
  sudo apt-get install -qq -y xvfb && echo -n "." || eval $ERROR
  sudo pip install -q pyvirtualdisplay && echo -n "." || eval $ERROR
  tar -xf unityVideo.tar.gz -C $HOME && echo -e ". $OK" || eval $ERROR
  rm unityVideo.tar.gz
fi

LIBRASDIR=$HOME/vlibras-contents/libras
VIDEOSDIR=$HOME/vlibras-contents/videos
FRAMESDIR=$HOME/vlibras-contents/frames

mkdir -p $LIBRASDIR $VIDEOSDIR $FRAMESDIR

echo -n "Creating environment variables... "
TEMP_ENV=$(mktemp)
echo "export VLIBRAS_VIDEO_CREATOR=$HOME/unityVideo/videoCreator.x86_64" > $TEMP_ENV
echo "export VLIBRAS_VIDEO_LIBRAS=$HOME/vlibras-contents/libras" >> $TEMP_ENV
echo "export VLIBRAS_VIDEO_MIXED=$HOME/vlibras-contents/videos" >> $TEMP_ENV
echo "export VLIBRAS_VIDEO_SCREENS=$HOME/vlibras-contents/frames" >> $TEMP_ENV
echo "export TEXT_FONTS=/usr/share/fonts/truetype/freefont/FreeSans.ttf" >> $TEMP_ENV
sudo install -m 755 $TEMP_ENV -D /etc/profile.d/vlibras-video-env.sh && echo -e $OK || eval $ERROR
rm -f TEMP_ENV
echo -e "Restart the computer to finalize installation."
