#!/usr/bin/env bash

OK="\033[32mOK\033[0m"
ERROR="echo -e '\033[31mERROR\033[0m'"
version=$(lsb_release -sr)

set -e

echo -n "Updating apt"
sudo apt-get -y -qq update && echo -e "... $OK" || eval $ERROR

echo -n "Installing linux images"
sudo apt-get install -y -qq linux-image-extra-$(uname -r) linux-image-extra-virtual && echo -e "... $OK" || eval $ERROR

echo -n "Installing HTTPS"
sudo apt-get install -y -qq apt-transport-https && echo -e "... $OK" || eval $ERROR

echo -n "Installing ca-certificates"
sudo apt-get install -y -qq ca-certificates && echo -e "... $OK" || eval $ERROR

echo -e "Adding keyserver..."
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D &>/dev/null

if [ "$version" = "16.04" ] || [ "$version" = "16.10" ]; then
	echo -e "Adding docker path to sources - VERSION: ${version}"
	echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
else
	echo -e "Adding docker path to sources - VERSION: ${version}"
	echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list
fi

echo -n "Updating apt"
sudo apt-get -y -qq update && echo -e "... $OK" || eval $ERROR

echo -n "Removing lxc"
sudo apt-get -y -qq purge lxc-docker && echo -e "... $OK" || eval $ERROR

echo -n "Caching docker-engine"
sudo apt-cache policy docker-engine &>/dev/null && echo -e "... $OK" || eval $ERROR

echo -n "Installing docker-engine"
sudo apt-get install -y -qq docker-engine && echo -e "... $OK" || eval $ERROR
