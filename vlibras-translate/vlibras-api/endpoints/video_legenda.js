var parameters = require('../helpers/parameters');
var properties = require('../helpers/properties');
var files = require('../helpers/files');
var db = require('../db/api');
var exec = require('child_process').exec, child;
var uuid = require('node-uuid');
var mkdirp = require('mkdirp');
var async = require('async');
var _ = require('lodash');
var logger = require('../logsystem/main.js');
var url = require('url');
var requests = require('../helpers/requests');
var querystring = require('querystring');

//Variaveis para conexao com a fila
var amqp = require('amqplib/callback_api');
var amqpconnection;

function init(req, res, Request) {
	res.set("Content-Type", "application/json");

	process(req, res, Request);
}

function process(req, res, Request) {
	var id = uuid.v4();
	var folder = properties.uploads_folder + id;
	var locals = {};

	var request_object = new Request({
		id: id,
		type: req.body.servico,
		status: 'Submitted',
		link: 'http://' + properties.SERVER_IP + ':' + properties.port + '/' + id + '.mp4',
		link_video: req.body.video_url,
		author: req.body.conteudista,
		callbackURL: req.body.callback,
		subtitle: 'http://150.165.204.80:5000/api/subtitle.srt',
		dictionary: '1.0',
		created_at: new Date(),
		updated_at: new Date(),
	});
	db.create(request_object, function(result) {
		if (result !== null) {
			res.send(200, { 'id': result.id});
		} else {
			res.send(500, { 'error': 'Erro na criação da requisição.'});
		}
	});

	async.series([
		// Cria a pasta apropriada
		function(callback) {
			console.log("== Criando pasta " + folder);

			mkdirp(folder, function(err) {
				var error;

				if (err) { error = "Erro na criação da pasta com o id: " + id + "; " + err; }

				callback(error);
			});
		},
		// Baixa e move os arquivos para a pasta correta
		function(callback) {
			console.log("== Baixando os arquivos");
			try {
				setTimeout(function() { downloadAndMoveFiles(folder, req, locals, callback); }, 1000);
			} catch (err) {
				console.log(" Erro " + err);
				callback(err);
			}
		},
		//Adicionando na fila
		function(callback) {
			console.log("== Adicionando na fila");
			try {
				setTimeout(function() { sendToQueue(request_object, req, locals, res, callback); }, 91000);
			} catch (err) {
				console.log(" Erro " + err);
				callback(err);
			}
		},
		//esperando a resposta da fila
		function(callback) {
			console.log("== Esperando na fila");
			try {
				setTimeout(function() { receiveFromQueue(request_object, Request, res, callback); }, 91000);
			} catch (err) {
				console.log(" Erro " + err);
				callback(err);
			}
		}
	], function(err) {
		// Se tiver erro
		if (err) {
			res.send(500, parameters.errorMessage(err));

			return;
		}
	});
	console.log("local do video: " + locals.video);
}

function downloadAndMoveFiles(folder, req, locals, callback) {
	async.parallel([
		// Download video
		function(callback) {
			console.log("Baixando video");
			files.downloadAndMoveVideo(folder, req, locals, callback);
		},

		// Download subtitle
		function(callback) {
			console.log("Baixando legenda");
			files.downloadAndMoveSubtitle(folder, req, locals, callback);
		}
	], function(err) {
		console.log("=== Legenda e video baixados");
		console.log("local do video: " + locals.video);
		// Callback chamado depois de todas as tarefas
		// Se tiver erro, vai passar para cima
		callback(err);
	});
}
//Funcao de adicionar na fila
function sendToQueue(request_object, req, locals ,res, callback){
			amqp.connect('amqp://rabbit', function(erro, conn) {
				if(erro != null){
					//res.json({message : erro});
					throw erro;
				}
				json = ({
							type: 'video',
							video : 'http://' + properties.SERVER_IP + ':' + properties.port + '/' + request_object.id + '/' + locals.video.path,
							subtitle : 'http://' + properties.SERVER_IP + ':' + properties.port + '/' + request_object.id + '/' + locals.subtitle.path,
							window_position : req.body.posicao,
							window_size : req.body.tamanho
						});
				console.log(request_object.id);
				console.log(json);
				conn.createChannel(function(err, ch) {
					if(err != null){
						res.json({message : err});
						throw err;
					}
					ch.assertQueue("requests", {
						durable : false
					});
					ch.sendToQueue("requests", new Buffer(JSON.stringify(json)) , {
						correlationId : request_object.id

					});
					callback();
					try {
					    ch.close();
					}
					catch (alreadyClosed) {
					    console.log(alreadyClosed.stackAtStateChange);
					}
					});
				setTimeout(function() { conn.close();}, 9500000);
				});
}

//Funcao de receber da fila
function receiveFromQueue(request_object, Request, res, callback){
	amqp.connect('amqp://rabbit', function(erro, conn) {
		if(erro != null){
			res.json({
				message : erro
			});
				throw erro;
		}

		conn.createChannel(function(err, ch) {
		    if(err != null){
				res.json({
					message : err
				});
					throw err;
			}
			ch.assertQueue("videos", {
				durable : false
			});
			ch.consume("videos", function(msg) {
				if (msg.properties.correlationId === request_object.id) {
					console.log(msg.properties.correlationId + " " + request_object.id);
					console.log(msg.content.toString());
					ch.ack(msg);
					// res.json({message : msg.content.toString()});
	                db.update(Request, request_object.id, 'Completed', function (result) {
		                logger.incrementService("videos", "traducoes");
		                data = JSON.stringify({
		                  'response' : 'http://' + properties.SERVER_IP + ':' + properties.port + '/' + request_object.id + '.mp4',
		                  'versao' : '1.0',
		                  'legenda' : ''
		                });
		                if (request_object.callbackURL !== undefined) {
		                	path = url.parse(request_object.callbackURL);
              				requests.postRequest(path, data);
		                }
	                });
					try {
					    ch.close();
					}
					catch (alreadyClosed) {
					    console.log(alreadyClosed.stackAtStateChange);
					}
				} else {
					ch.reject(msg);
				}
				}, {noAck : false});
			});
		setTimeout(function() {conn.close();}, 9500000);
		});
}
module.exports.init = init;
