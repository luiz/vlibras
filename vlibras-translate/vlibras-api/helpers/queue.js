var db = require('../db/api');
var exec = require('child_process').exec, child;
var requests = require('../helpers/requests');
var logger = require('../logsystem/main.js');
var properties = require('../helpers/properties');
var url = require('url');

exports.exec_command_line = function (command_line, callback, Request, idreq, res, id, done) {
      child = exec(command_line, function(err, stdout, stderr) {
      });

      child.on('disconnect', function(code, signal) {
        throw new Error("Disconectado do core");
      });

     // Endereço do callback
      if (callback !== undefined) {
        path = url.parse(callback);
      }
      
      /* Listener que dispara quando a requisição ao core finaliza */
      child.on('close', function(code, signal){
						// Se o core executou com erro
            if (code !== 0) {
              db.update(Request, idreq, 'Error', function (result) {});
              console.log("Erro no retorno do core. Código: " + code);
              logger.incrementError('core', "Erro no retorno do core. Código: " + code);
              data = {
                'error' : "Erro no Core",
                'code' : code,
                'id' : id
              };
            } else {
                db.update(Request, idreq, 'Completed', function (result) {});
                logger.incrementService("videos", "traducoes");
                data = {
                  'response' : 'http://' + properties.SERVER_IP + ':' + properties.port + '/' + id + '.mp4',
                  'versao' : '1.0',
                  'legenda' : ''
                };
	}

            if (callback !== undefined) {
              data = JSON.stringify(data);
              requests.postRequest(path, data);
            }
	done();

      });


      child.on('error', function(code, signal){
        db.update(Request, idreq, 'Error', function (result) {});
        console.log("Erro no retorno do core. Código: " + code);
        logger.incrementError('core', "Erro no retorno do core. Código: " + code);
        res.send(500, parameters.errorMessage('Erro na chamada ao core'));
	done();
      });

};

// use to debug
exports.text = function (text, callback) {
  console.log("Text inside queue_helper: " + text);
  return text;
};
