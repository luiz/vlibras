var host = process.env.VLIBRAS_VIDEO_IP;
var port = process.env.VLIBRAS_VIDEO_PORT;
var uploads_folder = 'uploads/';

module.exports.host = host;
module.exports.port = port;
module.exports.uploads_folder = uploads_folder;
module.exports.SERVER_IP = process.env.VLIBRAS_VIDEO_IP;
