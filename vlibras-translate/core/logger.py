#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Erickson Silva
E-Mail: erickson.silva@lavid.ufpb.br

Author: Jonathan Lincoln Brilhante
E-Mail: jonathan.lincoln.brilhante@gmail.com

Author: Wesnydy Lima Ribeiro
E-Mail: wesnydy@lavid.ufpb.br
"""

import graypy
import logging
import pika
import PikaManager
import sys

from time import sleep

SERVER_URL = sys.argv[1]

# Manager of queues connections.
manager = PikaManager.PikaManager("rabbit")

# Logging configuration.
logger = logging.getLogger('text_container')
logger.setLevel(logging.DEBUG)

handler = graypy.GELFHandler(SERVER_URL, 12201)
logger.addHandler(handler)

def run(ch, method, properties, body):
    print ("Writing log...")
    logger.debug(" [L] LOGGER  %r" % body)

def keep_alive(conn_send, conn_receive):
    """
    Keep the connection alive.

    Parameters
    ----------
    conn_send : object
        Connection of writer.
    conn_receive : object
        Connection of receiver.
    """
    while True:
        sleep(30)
        try:
            conn_send.process_data_events()
            conn_receive.process_data_events()
        except:
            continue

# start_new_thread(keep_alive, (manager.get_conn_send(), manager.get_conn_receive()))

print ("Logger listening...")
while True:
    try:
        manager.receive_from_queue("logs", run)
    except KeyboardInterrupt:
        manager.close_connections()
        os._exit(0)
